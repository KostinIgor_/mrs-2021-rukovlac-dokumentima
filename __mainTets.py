# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentima -> mainTets.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 17/12/2021 18:13
# @Desc   :
# '''=================================================
import pathlib
import random
import typing
from functools import reduce
from typing import Union

from PySide2 import QtGui
from PySide2.QtCore import Qt, QLineF, QRectF
from PySide2.QtGui import QPen, QBrush, QColor
from PySide2.QtWidgets import QMainWindow, QGraphicsScene, QGraphicsView, QGraphicsItem, \
    QGraphicsRectItem, QGraphicsPixmapItem, QGraphicsSceneMouseEvent, QGraphicsLineItem, QMessageBox
from multimethod import multimethod

from app.plugin_framework.subscription.subscriber import Subscriber
from app.plugin_framework.subscription.subscription import Subscription
from documentLoader.serializable.q_graphics_item import QGraphicsItemSerialization
from documentLoader.serializable.q_graphics_scene import QGraphicsSceneSerialization
from documentLoader.serializable.serializable import Serializable
from slot import Slot

point1 = None


class MyRect(QGraphicsRectItem, QGraphicsItemSerialization):  # Test primer
    def __init__(self, parent, x, y, w, h):
        super().__init__(x, y, w, h, parent)

        self.setBrush(QBrush(Qt.black))
        self.setFlag(QGraphicsItem.ItemIsMovable, True)
        self.setFlag(QGraphicsItem.ItemContainsChildrenInShape, True)


class MyImage(QGraphicsPixmapItem):  # Test prime
    def __init__(self, pixmap):
        super().__init__(pixmap)
        self.width = pixmap.width()
        self.height = pixmap.height()
        self.subscribers = []

    def setRect(self, x, y, w, h):
        self.setPos(x, y)

        self.resetMatrix()
        ws = w / self.width
        hs = h / self.height

        if self.width * ws <= w and self.height * ws <= h:
            self.setScale(ws)
            return

        if self.width * hs <= w and self.height * hs <= h:
            self.setScale(hs)

    def rect(self):
        return QRectF(self.pos().x(), self.pos().y(), self.width * self.scale(), self.height * self.scale())

    def selected(self):
        return "Selected item Image"


class Scene(QGraphicsScene, QGraphicsSceneSerialization, Subscription):
    REQUIRED_KEYS = ["items"]

    def __init__(self, width, height, manager=None):
        super().__init__()

        self.manager = manager
        self._subscriptions = []
        self._items = []

        # BORDER LINES
        self._border_pen = None

        self.top_line = QGraphicsLineItem()

        self.right_line = QGraphicsLineItem()

        self.bottom_line = QGraphicsLineItem()

        self.left_line = QGraphicsLineItem()

        self.addItem(self.top_line)
        self.addItem(self.right_line)
        self.addItem(self.bottom_line)
        self.addItem(self.left_line)

        self._items = []

        self.setSceneRect(0, 0, width, height)
        self.setBackgroundBrush(QBrush(Qt.white))

    @property
    def border_pen(self):
        return self._border_pen

    @border_pen.setter
    def border_pen(self, val: Union[QPen, QColor]):
        val = QPen(val) if type(val) is QColor else val
        self._border_pen = val

        self.top_line.setPen(self._border_pen)
        self.right_line.setPen(self._border_pen)
        self.bottom_line.setPen(self._border_pen)
        self.left_line.setPen(self._border_pen)

    def update_borders(self, rect=None):
        rect = rect if rect is not None else self.sceneRect()

        self.top_line.setLine(QLineF(rect.topLeft(), rect.topRight()))

        self.right_line.setLine(QLineF(rect.topRight(), rect.bottomRight()))

        self.bottom_line.setLine(QLineF(rect.bottomRight(), rect.bottomLeft()))

        self.left_line.setLine(QLineF(rect.topLeft(), rect.bottomLeft()))

    def items(self, order: Qt.SortOrder = ...) -> typing.List:
        return self._items

    def addItem(self, item: QGraphicsItem) -> None:
        self._emit("NewItem", item)
        if isinstance(item, Subscription):
            item.subscribe(Subscriber("DeleteItem", lambda _item: self.removeItem(_item)))
        self._items.append(item)
        super().addItem(item)

    def removeItem(self, item: QGraphicsItem) -> None:
        choice = QMessageBox.question(None, 'Delete Item', 'Are u sure you want to delete this item?',
                                      QMessageBox.Ok | QMessageBox.Cancel)

        if choice == QMessageBox.Ok:
            self.items().remove(item)
            super().removeItem(item)
        else:
            pass

    def serialize(self):
        super_dict = super().serialize()
        this_dict = {
            "items": [item.serialize() for item in self.items()]
        }
        return reduce(Serializable.update, (super_dict, this_dict), {})

    @multimethod
    def setSceneRect(self, rect: QRectF) -> None:
        super().setSceneRect(rect)
        self.update_borders(rect)

    @multimethod
    def setSceneRect(self, x: int, y: int, w: int, h: int):
        self.setSceneRect(QRectF(x, y, w, h))

    @staticmethod
    def deserialize(raw, manager):
        scene = Scene(0, 0, manager)
        scene._deserialize(raw)
        if Scene.validate(raw):
            for item in raw["items"]:
                scene.addItem(Slot.deserialize(item, None, manager))
        return scene

    def _deserialize(self, raw):
        super().deserialize(raw)

    def mouseDoubleClickEvent(self, event: QGraphicsSceneMouseEvent):
        if self.focusItem() is not None:
            print(f"Focused item is : {self.focusItem()}")
            self.focusItem().mouseDoubleClickEvent(event)
            return
        slot = Slot(event.scenePos().x(), event.scenePos().y(), 10, 10, None, None, self.manager)
        self.addItem(slot)
        slot.handleSelected = slot.handleBottomRight
        slot.mousePressRect = slot.boundingRect()
        slot.mousePressPos = event.scenePos()
        self.mouseMoveEvent = lambda _event: slot.interactiveResize(_event.scenePos())
        self.mouseReleaseEvent = self._mouseReleaseEvent

    def _mouseReleaseEvent(self, event: QGraphicsSceneMouseEvent) -> None:
        self.mouseMoveEvent = super().mouseMoveEvent
        self.mouseReleaseEvent = super().mouseReleaseEvent

    def prikazi(self, e):
        print("Pomeram se")

    # def mouseMoveEvent(self, event):
    #     global point1
    #     self.addRect(point1.x(), point1.y(), (point1.x() - event.pos().x()), (point1.y() - event.pos().y()))


def randomScene(width, height, manager=None):  # generator random scene
    images = [p.__str__() for p in pathlib.Path("app/assets/icon/icon8/Fluent").iterdir() if p.is_file()]
    scene = Scene(width, height, manager=None)
    scene.setBackgroundBrush(QBrush(Qt.white))

    # scene.addEllipse(random.randint(0, width - 200), random.randint(0, height - 200), 200, 200, QPen(Qt.green),
    #                  QBrush(Qt.red))
    # scene.addRect(random.randint(0, width - 200), random.randint(0, height - 200), 200, 200, QPen(Qt.black),
    #               QBrush(Qt.gray))

    # img_wig = QLabel()
    # img_wig.setPixmap(QPixmap().fromImage(QImage(images[random.randint(0, len(images) - 1)])))

    # create item in which the proxy lives
    # item = QGraphicsWidget()
    # scene.addItem(item)

    # create proxy with window and dummy content
    # proxy = QGraphicsProxyWidget()
    # proxy.setWidget(img_wig)
    # proxy.setScale(4)
    # proxy.setPos(random.randint(0, width-32), random.randint(0, height-32))
    # proxy.setFlag(QGraphicsItem.ItemIgnoresTransformations)

    # testW = random.randint(0, 300)
    # testh = random.randint(0, 300)
    # testItemSLot = Slot(random.randint(0, width - testW), random.randint(0, width - testh), testW, testh)

    # SIMPLE RECT ITEM TEST
    # item = QGraphicsRectItem()
    # item.setBrush(QBrush(Qt.black))
    #
    # rectP = Slot(random.randint(0, width - 32 * 4),
    #              random.randint(0, height - 32 * 4),
    #              random.randint(0, 300),
    #              random.randint(0, 400),
    #              item,
    #              None,
    #              manager)
    # rectP.setBrush(QBrush(Qt.red))
    #
    # scene.addItem(rectP)

    #  IMAGE ITEM TEST
    # img_wig = QImage(images[random.randint(0, len(images) - 1)])
    # image_width = img_wig.width()
    # image_height = img_wig.height()
    #
    # img_item = MyImage(QPixmap().fromImage(img_wig))
    #
    # img_slot = Slot(random.randint(0, width - image_width * 4),
    #                 random.randint(0, height - image_width * 4),
    #                 image_width,
    #                 image_height,
    #                 img_item,
    #                 None,
    #                 manager)
    #
    # scene.addItem(img_slot)

    # rect = QGraphicsRectItem(testItemSLot)
    #
    # rect.setRect(0, 0, 10, 10)
    # rect.setBrush(QBrush(Qt.red))
    # rect.setParentItem(testItemSLot)
    # item = MyRect(None, 50, 50, 200, 200)
    # scene.addItem(item)

    # EMPTY SLOT TEST
    width_empty = random.randint(0, width / 2)
    height_empty = random.randint(0, height / 2)
    EmptySlot = Slot(random.randint(0, width - width_empty),
                     random.randint(0, height - height_empty),
                     width_empty, height_empty,
                     None,
                     None,
                     manager)
    EmptySlot.setBrush(QBrush(Qt.blue))

    scene.addItem(EmptySlot)

    # EmptySlot2 = Slot(random.randint(0, width - width_empty),
    #                   random.randint(0, height - height_empty),
    #                   width_empty, height_empty,
    #                   None,
    #                   None,
    #                   manager)
    # EmptySlot2.setBrush(QBrush(Qt.blue))
    #
    # scene.addItem(EmptySlot2)

    return scene


def template1(width, height, manager=None):
    scene = QGraphicsScene()

    scene.setSceneRect(0, 0, width, height)

    # BORDER LINES
    topLine = QLineF(scene.sceneRect().topLeft(), scene.sceneRect().topRight())
    rightLine = QLineF(scene.sceneRect().topRight(), scene.sceneRect().bottomRight())
    bottomLine = QLineF(scene.sceneRect().bottomRight(), scene.sceneRect().bottomLeft())
    leftLine = QLineF(scene.sceneRect().topLeft(), scene.sceneRect().bottomLeft())

    pen = QPen(Qt.red)

    scene.addLine(topLine, pen)
    scene.addLine(rightLine, pen)
    scene.addLine(bottomLine, pen)
    scene.addLine(leftLine, pen)

    scene.setBackgroundBrush(QBrush(Qt.black))

    Naslov1 = Slot(20,
                   height / 2 - 70,
                   width - 40,
                   100,
                   None,
                   None,
                   manager)

    Naslov1.setBrush(QBrush(Qt.blue))

    scene.addItem(Naslov1)

    return scene


def template2(width, height, manager=None):
    scene = QGraphicsScene()

    scene.setSceneRect(0, 0, width, height)

    # BORDER LINES
    topLine = QLineF(scene.sceneRect().topLeft(), scene.sceneRect().topRight())
    rightLine = QLineF(scene.sceneRect().topRight(), scene.sceneRect().bottomRight())
    bottomLine = QLineF(scene.sceneRect().bottomRight(), scene.sceneRect().bottomLeft())
    leftLine = QLineF(scene.sceneRect().topLeft(), scene.sceneRect().bottomLeft())

    pen = QPen(Qt.red)

    scene.addLine(topLine, pen)
    scene.addLine(rightLine, pen)
    scene.addLine(bottomLine, pen)
    scene.addLine(leftLine, pen)

    Naslov2 = Slot(20,
                   20,
                   width - 40,
                   100,
                   None,
                   None,
                   manager)

    Naslov2.setBrush(QBrush(Qt.blue))

    Sadrzaj1 = Slot(20,
                    140,
                    width / 2 - 30,
                    height - 140 - 20,
                    None,
                    None,
                    manager)

    Sadrzaj1.setBrush(QBrush(Qt.blue))

    Sadrzaj2 = Slot(width / 2 + 20,
                    140,
                    width / 2 - 30,
                    height - 140 - 20,
                    None,
                    None,
                    manager)

    Sadrzaj2.setBrush(QBrush(Qt.blue))

    scene.addItem(Naslov2)
    scene.addItem(Sadrzaj1)
    scene.addItem(Sadrzaj2)

    return scene
def template3(width, height, manager=None):
    scene = QGraphicsScene()

    scene.setSceneRect(0, 0, width, height)

    # BORDER LINES
    topLine = QLineF(scene.sceneRect().topLeft(), scene.sceneRect().topRight())
    rightLine = QLineF(scene.sceneRect().topRight(), scene.sceneRect().bottomRight())
    bottomLine = QLineF(scene.sceneRect().bottomRight(), scene.sceneRect().bottomLeft())
    leftLine = QLineF(scene.sceneRect().topLeft(), scene.sceneRect().bottomLeft())

    pen = QPen(Qt.red)

    scene.addLine(topLine, pen)
    scene.addLine(rightLine, pen)
    scene.addLine(bottomLine, pen)
    scene.addLine(leftLine, pen)

    Sadrzaj3 = Slot(20,
                    20,
                    width / 3,
                    150,
                    None,
                    None,
                    manager)

    Sadrzaj3.setBrush(QBrush(Qt.blue))

    Sadrzaj4 = Slot(20,
                    180,
                    width / 3,
                    height - 200,
                    None,
                    None,
                    manager)

    Sadrzaj4.setBrush(QBrush(Qt.blue))

    Sadrzaj5 = Slot(width / 2 + 20,
                    20,
                    width / 2 - 20,
                    height - 40,
                    None,
                    None,
                    manager)

    Sadrzaj5.setBrush(QBrush(Qt.blue))

    scene.addItem(Sadrzaj3)
    scene.addItem(Sadrzaj4)
    scene.addItem(Sadrzaj5)

    return scene


class Window(QMainWindow):
    def __init__(self):
        super().__init__()
        self.scenes = []
        self.defaultScenes = []
        self.setMinimumWidth(800)
        self.setMinimumHeight(800)

        self.title = "PyQt5 QGraphicView"
        self.top = 0
        self.left = 0
        self.width = 800
        self.height = 800

        self.InitWindow()

    def InitWindow(self):
        self.setWindowIcon(QtGui.QIcon("icon.png"))
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        # for i in range(10):
        #    self.scenes.append(randomScene(self.width, self.height))

        self.defaultScenes = randomScene(self.width, self.height)  # template layouts

        graphicView = QGraphicsView(self.scenes[0], self)
        graphicView.setGeometry(self.top - 100, self.left - 100, self.width, self.height)
        graphicView.setSceneRect(self.top, self.left, self.width, self.height)

        graphicView.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        graphicView.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        graphicView.scale(0.3, 0.3)

        self.show()

# App = QApplication(sys.argv)
# window = Window()
# sys.exit(App.exec_())
