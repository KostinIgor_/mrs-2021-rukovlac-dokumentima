import os.path
from typing import Union

from PySide2 import QtWidgets
from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QAction, QStatusBar, QWidget

from app.UI.menu_bar import MenuBar
from app.UI.tool_bar import ToolBar
from app.UI.structure_dock import StructureDock
from app.UI.settings import Settings


class MainWindow(QtWidgets.QMainWindow):
    DEFAULT_ICON = f'app{os.path.sep}assets{os.path.sep}icon{os.path.sep}system{os.path.sep}document.png'

    def __init__(self, title: str, icon: Union[str, QIcon] = None, parent: QWidget = None):
        super().__init__(parent)
        self.setWindowTitle(title)
        _icon = (QIcon(icon) if type(icon) is str else icon) if icon is not None else QIcon(MainWindow.DEFAULT_ICON)
        self.setWindowIcon(_icon)
        self.resize(800, 600)

        # meni
        self.menu_bar = MenuBar(self, Settings)
        self.setthings= Settings(self)

        # toolbar
        self.tool_bar = ToolBar()
        self.tool_bar.addAction(QAction(_icon, "&New", self))

        # statusbar
        self.status_bar = QStatusBar(self)
        self.status_bar.showMessage("This is status bar (  Prototype )", 0)

        # centralized
        self.central_widget = QtWidgets.QStackedWidget(self)
        self.central_widget_widgets = []
        #self.central_widget.setStyleSheet("background-color: black;")
        # widgets in stacked widget
        self.widgets = {}

        self.addToolBar(self.tool_bar)
        self.setStatusBar(self.status_bar)
        self.setCentralWidget(self.central_widget)
        self.setMenuBar(self.menu_bar)

    def add_widget(self, widget):
        if not (widget in self.central_widget_widgets):
            self.central_widget.addWidget(widget)
        self.central_widget.setCurrentWidget(widget)

    def remove_widget(self, widget=None):
        if widget is None:
            for index in range((self.central_widget.count() - 1), -1, -1):
                widget_to_remove = self.central_widget.widget(index)
                widget_to_remove.hide()
                self.central_widget.removeWidget(widget_to_remove)
                widget_to_remove.deleteLater()

            return
        self.central_widget.removeWidget(widget)

    def create_structure(self):
        structure_dock=StructureDock("Strukture dokumenata", self.main_window)
        self.main_window.addDockWidget(QtCore.Qt.LeftDockWidgetArea, structure_dock)
        self.structure_dock.resize(400, 300)

    # TODO: dodati metodu koja ce u fokus staviti trenutko aktivni plugin (njegov widget)
