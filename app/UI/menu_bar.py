#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：mrs-2021-rukovlac-dokumentima 
@File ：menu_bar.py
@Author ：Igor Kostin ( 2019270544 ) - KostinIgor_ (gitlab)
@Date ：11/29/21 , 7:20 PM
"""
from PySide2 import QtWidgets
from PySide2.QtWidgets import QMenuBar, QMenu, QAction, QFileDialog
from PySide2 import QtPrintSupport
from app.UI.settings import Settings
import sys

from PySide2 import QtPrintSupport, QtCore
from PySide2.QtWidgets import QMenuBar, QMenu, QAction, QFileDialog


class MenuBar(QMenuBar):
    clicked = QtCore.Signal(str)
    save = QtCore.Signal(bool)
    delete = QtCore.Signal()
    new = QtCore.Signal()

    def __init__(self, parent, Settings):
        super().__init__(parent)

        self.file_menu = QMenu("File", self)
        self.edit_menu = QMenu("Edit", self)
        self.view_menu = QMenu("View", self)
        self.help_menu = QMenu("Help", self)

        # FILE MENU___________________________________

        new_action = QAction("New File", self)
        new_action.setShortcut('Ctrl+N')
        new_action.triggered.connect(self.new_file)

        open_action = QAction("Open File", self)
        open_action.setShortcut('Ctrl+O')
        open_action.triggered.connect(self.open_file)

        open_folder = QAction("Open Folder(UnderConstruction)", self)
        open_folder.setShortcut('Ctrl+K+O')

        save_action = QAction("Save", self)
        save_action.setShortcut('Ctrl+S')
        save_action.triggered.connect(self.save_file)

        print_action = QAction("Print(UnderConstruction)", self)
        print_action.setShortcut('Ctrl+P')
        print_action.triggered.connect(self.print_dialog)

        exit_action = QAction("Exit", self)
        exit_action.triggered.connect(self.exit_app)

        self.file_menu.addAction(new_action)
        self.file_menu.addAction(open_action)
        self.file_menu.addAction(open_folder)
        self.file_menu.addAction(save_action)
        self.file_menu.addAction(print_action)
        self.file_menu.addAction(exit_action)

        # _____________________________________________

        # EDIT MENU____________________________________

        undo_action = QAction("Undo(UnderConstruction)", self)
        undo_action.setShortcut('Ctrl+Z')

        cut_action = QAction("Cut(UnderConstruction)", self)
        cut_action.setShortcut('Ctrl+X')

        find_action = QAction("Find(UnderConstruction)", self)
        find_action.setShortcut('Ctrl+F')

        copy_action = QAction("Copy(UnderConstruction)", self)
        copy_action.setShortcut('Ctrl+C')

        paste_action = QAction("Paste(UnderConstruction)", self)
        paste_action.setShortcut('Ctrl+V')

        settings_action = QAction("Settings", self)
        settings_action.setShortcut('Ctrl+Q')
        settings_action.triggered.connect(self.open_settings)

        delete_action = QAction("Delete", self)
        delete_action.setShortcut('Ctrl+D')
        delete_action.triggered.connect(lambda: self.delete.emit())

        self.edit_menu.addAction(undo_action)
        self.edit_menu.addAction(cut_action)
        self.edit_menu.addAction(copy_action)
        self.edit_menu.addAction(paste_action)
        self.edit_menu.addAction(find_action)
        self.edit_menu.addAction(delete_action)

        # ___________________________________________

        about = QAction("About", self)
        self.help_menu.addAction(about)
        self.help_menu.addAction(settings_action)

        self.addMenu(self.file_menu)
        self.addMenu(self.edit_menu)
        self.addMenu(self.view_menu)
        self.addMenu(self.help_menu)

    def save_file(self):
        # fn, _ = QFileDialog.getSaveFileName(self, "Save File", None, "PDF files (.pdf); All files")
        #
        # if fn != '':
        #
        #     if QFileInfo(fn).suffix() == "": fn += '.pdf'
        #
        #     printer = QtPrintSupport.QPrinter(QtPrintSupport.QPrinter.HighResolution)
        #     printer.setOutputFormat(QtPrintSupport.QPrinter.PdfFormat)
        #     printer.setOutputFileName(fn)
        # self.workspace.document().print_(self.printer)
        self.save.emit(True)

    def print_dialog(self):
        printer = QtPrintSupport.QPrinter(QtPrintSupport.QPrinter.HighResolution)
        dialog = QtPrintSupport.QPrintDialog(printer, self)

        if dialog.exec_() == QtPrintSupport.QPrintDialog.Accepted:
            self.textEdit.print_(printer)

    def open_file(self):
        path_to_file, _ = QFileDialog.getOpenFileName(self, self.tr("Load File"), self.tr("~/Desktop/"),
                                                      self.tr("DOCXY (*.docxy)"))
        self.clicked.emit(path_to_file)

    def open_settings(self):
        self.window = Settings(self)
        self.window.show()

    def exit_app(self):
        sys.exit()

    def new_file(self):
        self.new.emit()
