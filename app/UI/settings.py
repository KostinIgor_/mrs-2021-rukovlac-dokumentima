from PySide2 import QtWidgets, QtGui, QtCore
from PySide2.QtWidgets import QMainWindow
from app.UI.ui_settings import Ui_Settings


class Settings(QtWidgets.QMainWindow):
    def __init__(self, main_window, parent=None):
        super().__init__(parent)
        self.main_window = main_window
        self.settings = Ui_Settings()
        self.settings.setupUi(self)

        ## UI ==> INTERFACE CODES
        ########################################################################

        ## REMOVE TITLE BAR
        # self.setWindowFlag(QtCore.Qt.FramelessWindowHint)
        # self.setAttribute(QtCore.Qt.WA_TranslucentBackground)



        ## SHOW ==> MAIN WINDOW
        ########################################################################
        def open_settings(self):

            self.show()
        ## ==> END ##
        