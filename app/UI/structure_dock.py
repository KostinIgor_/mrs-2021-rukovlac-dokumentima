import os
from pathlib import Path

from PySide2 import QtWidgets, QtCore


class StructureDock(QtWidgets.QDockWidget):
    clicked = QtCore.Signal(str)  # Atribut klase

    def __init__(self, title, parent):
        super().__init__(title, parent)

        self.model = QtWidgets.QFileSystemModel()
        self.model.setNameFilters({"*.docxy"})
        self.model.setNameFilterDisables(False)
        self.model.setRootPath(QtCore.QDir.currentPath() + os.path.sep + "data")

        self.tree = QtWidgets.QTreeView()
        self.tree.setModel(self.model)
        self.tree.setRootIndex(self.model.index(QtCore.QDir.currentPath() + os.path.sep + "data"))
        self.tree.clicked.connect(self.file_clicked)
        self.setWidget(self.tree)
        # self.kliknut = QtCore.Signal(str)

    def file_clicked(self, index):
        print(self.model.filePath(index))
        path = self.model.filePath(index)
        self.clicked.emit(path)
