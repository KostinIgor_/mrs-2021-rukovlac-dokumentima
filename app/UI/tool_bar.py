#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：mrs-2021-rukovlac-dokumentima 
@File ：tool_bar.py
@Author ：Igor Kostin ( 2019270544 ) - KostinIgor_ (gitlab)
@Date ：11/30/21 , 12:39 PM
"""
from PySide2.QtWidgets import QToolBar


class ToolBar(QToolBar):
    def __init__(self):
        super().__init__()
