# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentima -> manager.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 17/12/2021 18:30
# @Desc   :
# '''=================================================

class PluginManager:
    def __init__(self, model):
        self.model = model

    def get(self, x):
        return self.model.get(x)

    def get_by_extension_point(self, extension_point_id: int):
        return self.model.get_by_extension_point(extension_point_id)
