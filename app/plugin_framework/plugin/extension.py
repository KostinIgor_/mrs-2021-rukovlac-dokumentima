from abc import ABC

from app.plugin_framework.plugin.spec.plugin_specification import PluginSpecification


class Extension(ABC):
    def __init__(self, specification: PluginSpecification, iface):
        self._specification = specification
        self.iface = iface

    @property
    def id(self):
        return self._specification.id

    @property
    def name(self):
        return self._specification.name

    @property
    def authors(self):
        return self._specification.authors

    @property
    def version(self):
        return self._specification.version

    @property
    def core_version(self):
        return self._specification.version

    @property
    def extension_point(self):
        return self._specification.extension_point

    @property
    def licence(self):
        return self._specification.licence

    @property
    def description(self):
        return self._specification.description

    @property
    def web_page(self):
        return self._specification.web_page

    @property
    def dependencies(self):
        return self._specification.dependencies

    @property
    def labels(self):
        return self._specification.labels

    @property
    def short_overview(self):
        return self._specification.short_overview

    @property
    def icon(self):
        return self._specification.icon

    @property
    def path(self):
        return self._specification.path
