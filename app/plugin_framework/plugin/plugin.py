from abc import ABC, abstractmethod

from app.plugin_framework.plugin.extension import Extension


class Plugin(Extension, ABC):

    @abstractmethod
    def activate(self):
        pass

    @abstractmethod
    def deactivate(self): ...  # skraceno za pass (ako nemamo telo)
