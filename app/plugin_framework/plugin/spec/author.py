from abc import ABC

from app.plugin_framework.plugin.spec.specification import Specification


class Author(Specification, ABC):
    REQUIRED_KEYS = ['first_name', 'last_name', 'email', 'web_page']

    def __init__(self, first_name: str, last_name: str, email: str, web_page: str, image: str = None):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.web_page = web_page
        self.image = image  # This attribute will not be loaded into object until view is needed

    def show(self, parent):
        pass
