#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：mrs-2021-rukovlac-dokumentima 
@File ：_Description.py
@Author ：Igor Kostin ( 2019270544 ) - KostinIgor_ (gitlab)
@Date ：11/30/21 , 9:52 PM
"""
from abc import ABC

from app.plugin_framework.plugin.spec.specification import Specification


class _Description(Specification, ABC):
    REQUIRED_KEYS = ["path"]

    def __init__(self, path: str):
        self.path = path
        self.text = None  # This attribute will be loaded into object at it's initialisation.

    def load(self):
        pass

    def show(self, parent):
        ...
