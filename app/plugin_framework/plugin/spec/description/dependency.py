from abc import ABC

from app.plugin_framework.plugin.spec.specification import Specification


class Dependency(Specification, ABC):
    def __init__(self, _id, version):
        self.id = _id
        self.version = version
