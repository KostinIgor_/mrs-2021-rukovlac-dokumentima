#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：mrs-2021-rukovlac-dokumentima 
@File ：descriptionFactory.py
@Author ：Igor Kostin ( 2019270544 ) - KostinIgor_ (gitlab)
@Date ：11/30/21 , 9:56 PM
"""
from pathlib import Path

from app.plugin_framework.plugin.spec.description._description import _Description
from app.plugin_framework.plugin.spec.description.markodownDescription import MarkdownDescription
from app.plugin_framework.plugin.spec.description.textDescription import TextDescription


class MarkupDescription:
    pass


class DescriptionFactory:
    DEFAULT_SUPPORTED_TYPES = ['.txt', '.md']

    @staticmethod
    def create(description_dict: dict):
        if description_dict is None:
            raise ValueError("Path for description is not provided!!")

        # TODO - error - nije dict
        path = Path(description_dict["path"])

        # if not path.is_file():
        #     raise ValueError(f"The provided path for description '{path}' is not file")

        # if path.suffix not in DescriptionFactory.DEFAULT_SUPPORTED_TYPES:
        #     raise ValueError(f"The provided path for description '{path}' leads to invalid file type,"
        #                      "supported file types are {DescriptionFactory.DEFAULT_SUPPORTED_TYPES} but "
        #                      "provided path lead to file that is '{path.suffix}'")

        if path.suffix == '.txt':
            return TextDescription.create(description_dict)

        if path.suffix == '.md':
            return MarkdownDescription.create(description_dict)

        empty_description = _Description("")
        empty_description.text = "This plugin has no description ."
        return empty_description
