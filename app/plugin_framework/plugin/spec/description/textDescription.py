#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：mrs-2021-rukovlac-dokumentima 
@File ：textDescription.py
@Author ：Igor Kostin ( 2019270544 ) - KostinIgor_ (gitlab)
@Date ：11/30/21 , 10:07 PM
"""
from abc import ABC

from app.plugin_framework.plugin.spec.description._description import _Description


class TextDescription(_Description, ABC):
    def __init__(self, path: str):
        super().__init__(path)

    def show(self, parent): ...
