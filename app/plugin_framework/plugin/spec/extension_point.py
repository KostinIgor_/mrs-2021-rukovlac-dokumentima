# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentimaC -> extension_point.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 29/12/2021 18:58
# @Desc   :
# '''=================================================
from abc import ABC

from app.plugin_framework.plugin.spec.specification import Specification


class ExtensionPoint(Specification, ABC):
    REQUIRED_KEYS = ['_id', 'name']

    def __init__(self, _id: int, name: str):
        self._id = _id
        self._name = name

    @property
    def id(self):
        return self._id

    @property
    def name(self):
        return self._name

    def show(self, parent):
        pass
