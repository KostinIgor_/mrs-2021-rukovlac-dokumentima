#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：mrs-2021-rukovlac-dokumentima 
@File ：label.py
@Author ：Igor Kostin ( 2019270544 ) - KostinIgor_ (gitlab)
@Date ：11/30/21 , 9:21 PM
"""
from abc import ABC

from app.plugin_framework.plugin.spec.specification import Specification


class Label(Specification, ABC):
    REQUIRED_KEYS = ["color", "text", 'icon']

    def __init__(self, color: str, text: str, icon: str):
        self.color = color
        self.text = text
        self.icon = icon

    def show(self, parent): ...
