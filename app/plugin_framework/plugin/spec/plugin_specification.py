from pathlib import Path

from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QWidget, QLabel, QGridLayout, QPushButton

from app.plugin_framework.plugin.spec.specification_wrap import PluginSpecificationWrap


class PluginSpecification(PluginSpecificationWrap):
    def __init__(self, _id, name, authors, version, core_version, extension_point, licence, description, web_page,
                 dependencies, labels, short_overview, icon):
        super().__init__(_id, name, authors, version, core_version, extension_point, licence, description, web_page,
                         dependencies, labels, short_overview, icon)

        self._overview = None
        self.valid = None

    def set_valid(self, param: bool):
        self.valid = param

    def show(self, parent):
        pass

    def overview(self, parent: QWidget) -> QWidget:
        """
        The method returns a overview of the plugin specifications, if this is the first call of this method,
        it means that the overview has not been generated yet, so it will be generated and saved for future use.

        :param parent: Parent widget
        :return: Overview widget.
        """
        print('generated ovewrview')
        return self._gen_overview(parent) if self._overview is None else self._overview

    def _gen_overview(self, parent: QWidget) -> QWidget:
        wig = QWidget(parent)

        # PLUGIN ICON SETUP
        icon = QWidget(wig)
        icon.setStyleSheet(f"border-image: url({Path('app/assets/icon/icon8/Fluent/icons8-plugin-32.png').__str__()}) "
                           f"0 0 0 0 stretch stretch;"
                           f"background-color: white")

        name = QLabel(self.name, parent)
        name.setStyleSheet("background-color:yellow;")
        short_overview = QLabel(self.short_overview, parent)
        #short_overview.setStyleSheet("background-color:yellow;")

        # PLUGIN ICON SETUP
        btn_download = QPushButton(QIcon(Path('app/assets/icon/icon8/Fluent/icons8-download-32.png').__str__()), '',
                                   wig)
        btn_download.setStyleSheet("background-color:;")

        # GRID setup
        grid = QGridLayout()
        #                 r  c  rs cs
        grid.addWidget(icon, 0, 0, 2, 3)
        grid.addWidget(name, 0, 3, 1, 21)
        grid.addWidget(short_overview, 1, 3, 1, 21)
        grid.addWidget(btn_download, 0, 24, 2, 2)

        grid_column_width = round(wig.parent().size().width() / grid.columnCount() / 2)

        btn_download.setFixedSize(2 * grid_column_width, 2 * grid_column_width)
        icon.setFixedSize(3 * grid_column_width, 3 * grid_column_width)

        wig.setLayout(grid)
        return wig
