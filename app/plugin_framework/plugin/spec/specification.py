#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：mrs-2021-rukovlac-dokumentima 
@File ：specifcation.py
@Author ：Igor Kostin ( 2019270544 ) - KostinIgor_ (gitlab)
@Date ：11/30/21 , 5:49 PM
"""
from abc import ABC, abstractmethod

from PySide2.QtWidgets import QWidget


class Specification(ABC):

    @classmethod
    def create(cls, provided_dict: dict):
        """
        This is class method that creates a specification object for a given class (cls).
        This method will check to see if the passed object is valid for creating the required class

        :param provided_dict: python raw dict for cls
        :return: Specification
        """
        if not cls.validate(provided_dict):
            raise ValueError(f"Missing keys {cls._get_missing_keys(provided_dict)} in '{provided_dict['name']}' plugin")
        return cls(**provided_dict)

    @classmethod
    def validate(cls, provided_dict: dict) -> bool:
        """
        Validate forwarded object ( provided_dict ) for a given class (cls),
        Checks that all REQUIRED_KEYS are in that objet ( provided_dict ), so the object
        can be used to construct specification class.

        :param provided_dict: python raw dict for cls
        :return: bool
        """
        return all(key in provided_dict for key in cls.REQUIRED_KEYS)

    @classmethod
    def _get_missing_keys(cls, provided_dict: dict) -> list:
        """
        Supplies all keys that are missing in the passed object ( provided_dict ), for a given class (cls)

        :param provided_dict:
        :return: list of missing keys
        """
        return [key for key in cls.REQUIRED_KEYS if key not in provided_dict]

    @abstractmethod
    def show(self, parent: QWidget) -> QWidget:
        """
        Abstract method, that will be implemented in inherited class, which return
        QWidget object that represent a view for that particular specification .

        :param parent: parent widget in which will se added new widget
        """
        raise NotImplementedError
