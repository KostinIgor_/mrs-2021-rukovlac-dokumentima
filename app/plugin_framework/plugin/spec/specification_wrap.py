# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentimaC -> specification_wrap.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 29/12/2021 20:13
# @Desc   :
# '''=================================================
from abc import ABC
from pathlib import Path

from app.plugin_framework.plugin.spec.author import Author
from app.plugin_framework.plugin.spec.description.dependency import Dependency
from app.plugin_framework.plugin.spec.description.descriptionFactory import DescriptionFactory
from app.plugin_framework.plugin.spec.extension_point import ExtensionPoint
from app.plugin_framework.plugin.spec.label import Label
from app.plugin_framework.plugin.spec.specification import Specification


class PluginSpecificationWrap(Specification, ABC):
    DEFAULT_SPECIFICATION_NAME = "specification.json"
    REQUIRED_KEYS = ['_id', 'name', 'authors', 'version', 'core_version', 'extension_point', 'licence',
                     'short_overview',
                     'description', 'labels', 'dependencies', 'web_page', 'icon']

    def __init__(self, _id, name, authors, version, core_version, extension_point, licence, description, web_page,
                 dependencies, labels, short_overview, icon):
        self._id = _id
        self._name = name
        self._authors = [Author.create(author) for author in authors]
        self._version = version
        self._core_version = core_version
        self._extension_point = ExtensionPoint.create(extension_point)
        self._licence = licence
        self._description = DescriptionFactory.create(description)
        self._web_page = web_page
        self._dependencies = [Dependency.create(dependency) for dependency in dependencies]
        self._labels = [Label.create(label) for label in labels]
        self._short_overview = short_overview
        self._icon = icon

        self._path = None

    @property
    def id(self):
        return self._id

    @property
    def name(self):
        return self._name

    @property
    def authors(self):
        return self._authors

    @property
    def version(self):
        return self._version

    @property
    def core_version(self):
        return self._core_version

    @property
    def extension_point(self):
        return self._extension_point

    @property
    def licence(self):
        return self._licence

    @property
    def description(self):
        return self._description

    @property
    def web_page(self):
        return self._web_page

    @property
    def dependencies(self):
        return self._dependencies

    @property
    def labels(self):
        return self._labels

    @property
    def short_overview(self):
        return self._short_overview

    @property
    def icon(self):
        return self._icon

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, val: Path):
        self._path = val
