# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs2021rukovlacDokumentima-EngineBase -> state.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 06/12/2021 22:54
# @Desc   : Different states of plugin, with additional PluginMode that offers only ENABLED and DISABLED state
# '''=================================================
from enum import Enum


class _State(str, Enum):
    """
    Base state for all different states of plugin.
    """
    ENABLED = "ENABLED"  # Plugin is installed and enabled
    DISABLED = "DISABLED"  # Plugin is disabled
    LOADED = "LOADED"  # Plugin's class is loaded
    INSTALLED = "INSTALLED"  # Plugins _main class is CREATED
    FAILED = "FAILED"  # Plugin have failed

    def __str__(self):
        return self.value


class PluginMode(Enum):
    """
    Different mode that plugin could be ( ENABLED and DISABLED )
    """
    ENABLED = _State.ENABLED  # Plugin is installed and enabled
    DISABLED = _State.DISABLED  # Plugin is disabled

    def __eq__(self, other):
        return self.value == other.value

    def __str__(self):
        return self.value.__str__()


class PluginState(Enum):
    """
    Different states of plugin
    """
    ENABLED = _State.ENABLED  # Plugin is installed and enabled
    DISABLED = _State.DISABLED  # Plugin is disabled
    LOADED = _State.LOADED  # Plugin's class is loaded
    INSTALLED = _State.INSTALLED  # Plugins _main class is CREATED
    FAILED = _State.FAILED  # Plugin have failed

    def __eq__(self, other):
        return self.value == other.value

    def __str__(self):
        return self.value.__str__()

# print(PluginMode.DISABLED == PluginState.DISABLED)
# print(PluginMode.DISABLED is PluginState.DISABLED)
# print(PluginState.DISABLED == PluginMode.DISABLED)
# print(PluginState.DISABLED is PluginMode.DISABLED)
# print(f'{PluginMode.DISABLED} is type of {type(PluginMode.DISABLED.value)}')
# print(PluginMode.ENABLED)
# print(_State.DISABLED)
