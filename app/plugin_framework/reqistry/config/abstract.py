# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs2021rukovlacDokumentima-EngineBase -> abstract_plugin_config.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 12/12/2021 14:18
# @Desc   : Abstract class for plugin configuration, that implements all methods for working
#           with plugins configuration except tha method that
#           returns PluginConfigLoader ( that method will be implemented in child classes )
# '''=================================================
from abc import ABC, abstractmethod
from pathlib import Path

from app.plugin_framework.plugin.state import PluginMode
from app.plugin_framework.reqistry.config.configuration import RegistryPluginsConfiguration
from app.plugin_framework.reqistry.config.loader.loader import PluginConfigLoader


# TODO: odrediti kad ce se izrsavati zapis izmena


class AbstractPluginConfig(RegistryPluginsConfiguration, ABC):
    def __init__(self):
        self.loader = self._loader()
        self.config = self.loader.load()

    @abstractmethod
    def _loader(self) -> PluginConfigLoader:
        """
        Internal method that returns a loader for configuration
        """
        raise NotImplementedError

    def add(self, plugin_id: str, package: Path, mode: PluginMode = PluginMode.DISABLED) -> bool:
        self.config.append((plugin_id, package, mode))
        return True

    def remove(self, plugin_id: str) -> bool:
        # TODO - error - ne postoji plagin sa datim id-jem
        self.config.remove(self.config.get(plugin_id))
        return True

    def enable(self, plugin_id: str) -> bool:
        # TODO: error - plugin sa prosledjenim id-jem nije pronadjen
        # TODO : mishandling - plagin je vec omogucen ( pokusaj postavljnja
        #  novog stanja koje je isto koa i staro )

        t = self.config.get(plugin_id)
        index = self.config.configs.index(t)

        self.config.configs[index] = t[0], t[1], PluginMode.ENABLED

        return True

    def disable(self, plugin_id: str) -> bool:
        # TODO: error - plugin sa prosledjenim id-jem nije pronadjen
        # TODO : mishandling - plagin je vec onemogucen ( pokusaj postavljnja
        #  novog stanja koje je isto koa i staro )

        t = self.config.get(plugin_id)
        index = self.config.configs.index(t)

        self.config[index] = PluginMode.DISABLED

        return True
