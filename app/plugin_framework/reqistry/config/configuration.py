# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs2021rukovlacDokumentima-EngineBase -> plugins_configuration.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 12/12/2021 01:08
# @Desc   : Interface for registry configuration for plugins and their state
# '''=================================================
from abc import ABC, abstractmethod
from pathlib import Path

from app.plugin_framework.plugin.state import PluginMode


class RegistryPluginsConfiguration(ABC):

    @abstractmethod
    def add(self, plugin_id: str, package: Path, mode: PluginMode = PluginMode.DISABLED) -> bool:
        """
        Adds new plugin to configuration

        :param plugin_id: Plugin's Id
        :param package: Plugin's package ( probably the a in the file system )
        :param mode: Plugin's mode ( PluginMode ), the default mode is DISABLED, because it is assumed that this
                      method will be called after loading the plugin, but before its installation, therefore, in
                      terms of configuration, the plugin is disabled
        """
        raise NotImplementedError

    @abstractmethod
    def remove(self, plugin_id: str) -> bool:
        """
        Removes the plugin with id that is provided from configuration

        :param plugin_id: Plugin's Id
        """
        raise NotImplementedError

    @abstractmethod
    def enable(self, plugin_id: str) -> bool:
        """
        Changes the mode of the plugin with the given id in PluginMode.ENABLED

        :param plugin_id: Plugin's Id
        """
        raise NotImplementedError

    @abstractmethod
    def disable(self, plugin_id: str) -> bool:
        """
        Changes the mode of the plugin with the given id in PluginMode.DISABLED

        :param plugin_id: Plugin's Id
        """
        raise NotImplementedError
