# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs2021rukovlacDokumentima-EngineBase -> default.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 12/12/2021 16:15
# @Desc   : Default plugins configurator
# '''=================================================
from abc import ABC

from app.plugin_framework.reqistry.config.abstract import AbstractPluginConfig
from app.plugin_framework.reqistry.config.loader.FileSystemConfigLoader import FileSystemConfigLoader


class DefaultPluginConfigurator(AbstractPluginConfig, ABC):
    def _loader(self):
        return FileSystemConfigLoader()
