# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs2021rukovlacDokumentima-EngineBase -> FileSystemConfigLoader.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 12/12/2021 16:16
# @Desc   : Default plugin configuration loader, that loads and also saves plugins configuration in local file system
# '''=================================================
import json
from abc import ABC
from pathlib import Path

from app.plugin_framework.plugin.state import PluginMode
from app.plugin_framework.reqistry.config.loader.loader import PluginConfigLoader
from app.plugin_framework.reqistry.config.model import PluginsConfigModel

info = tuple[str, Path, PluginMode]


class FileSystemConfigLoader(PluginConfigLoader, ABC):
    DEFAULT_CONFIG = Path('app/config/registry/pluginState.json')

    def load(self, **kwargs):
        # TODO: error - sta ako fajl ne postoji, ucitavanje nije uspelo ili parsiranje nije uspelo

        with open(self.DEFAULT_CONFIG, 'r') as cnf:
            return PluginsConfigModel(self.json_parser(json.load(cnf)))

    def write(self, model, **kwargs):
        # TODO: error - fajl ne postoji, parsiranje nije uspelo, upisivanje nije uspelo

        with open(self.DEFAULT_CONFIG, 'w') as cnf:
            json.dump(self.reverse_json_parser(model), cnf, ensure_ascii=False, indent=4)

    @staticmethod
    def json_parser(data: dict) -> list[info]:
        """
        Transforms dict into list of tuples(plugin_id, package, mode)

        :param data: Python raw dict ( loaded from pluginState.json ) that need to be converted into list of tuples
        :return:
        """
        return [(i["plugin_id"], Path(i["package"]), PluginMode[i["mode"]]) for i in data]

    @staticmethod
    def reverse_json_parser(model: PluginsConfigModel) -> list[dict]:
        """
        Transforms model ( list of tuples ) into python dict object that is suitable for writing in local
         file system as json file

        :param model: Configuration model to be saved
        """
        return [{"plugin_id": i[0],  # i[0] is value at first position in info tuple that is plugin_id
                 "package":   i[1].__str__(),  # i[1] is Path(package),
                 "mode":      i[2].__str__()  # i[2] is PluginMode
                 } for i in model]
