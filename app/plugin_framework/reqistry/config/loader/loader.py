# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs2021rukovlacDokumentima-EngineBase -> loader.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 12/12/2021 14:41
# @Desc   : Interface for plugin's config loader
# '''=================================================
from abc import ABC, abstractmethod

from app.plugin_framework.reqistry.config.model import PluginsConfigModel


class PluginConfigLoader(ABC):

    @abstractmethod
    def load(self, **kwargs) -> PluginsConfigModel:
        """
        Loads PluginConfigModel ( configuration for plugins state, useful in PluginRegistry )

        :param kwargs: Possible parameters, that may be required in concrete implementation.
        """
        raise NotImplementedError

    @abstractmethod
    def write(self, model: PluginsConfigModel, **kwargs):
        """
        Saves provided Model for future use

        :param model: Model of PluginConfigModel which should be preserved
        :param kwargs: Possible parameters, that may be required in concrete implementation.
        """
        raise NotImplementedError
