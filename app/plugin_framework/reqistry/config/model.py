# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs2021rukovlacDokumentima-EngineBase -> config_model.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 12/12/2021 14:48
# @Desc   : Model for plugins configuration, with additional method for easier use
# '''=================================================
from abc import ABC
from pathlib import Path

from multimethod import multimethod

from app.plugin_framework.plugin.state import PluginMode
from app.plugin_framework.reqistry.config.model_I import PluginConfigList

info = tuple[str, Path, PluginMode]


class PluginsConfigModel(PluginConfigList, ABC):
    def __init__(self, configs: list[info]):
        self.configs = configs

    # interface method
    def __iter__(self):
        for c in self.configs:
            yield c

    def __getitem__(self, item):
        return self.configs[item]

    def get(self, plugin_id):
        ids = self.ids()
        if plugin_id in ids:
            return self[ids.index(plugin_id)]
        return None

    def all(self, mode):
        return [i for i in self.configs if i[2] == mode]

    @multimethod
    def ids(self):
        return [i[0] for i in self.configs]

    @multimethod
    def ids(self, mode: PluginMode):
        return [i[0] for i in self.all(mode)]

    @multimethod
    def package(self) -> list[Path]:
        return [i[1] for i in self.configs]

    @multimethod
    def package(self, mode: PluginMode):
        return [i[1] for i in self.all(mode)]

    # Built ins
    def __setitem__(self, key: int, mode: PluginMode):
        """
        Setter for changing the value of plugin's mode, assumption is that only mode of plugin could be changed

        :param key: Index of plugins configuration
        :param mode: New mode of plugin
        """
        t = self[key]
        self.configs[key] = t[0], t[1], mode

    def append(self, plugin: info):
        self.configs.append(plugin)

    def remove(self, plugin: info):
        self.configs.remove(plugin)

# TESTING
# x = PluginsConfigModel([("1123", Path("/"), PluginMode.ENABLED),
#                         ("5465", Path("/6dghdg"), PluginMode.ENABLED),
#                         ("763", Path("/dhgd"), PluginMode.DISABLED),
#                         ("237", Path("/dhgd"), PluginMode.DISABLED),
#                         ("11hgd23", Path("/dgghstgdsh"), PluginMode.ENABLED),
#                         ("ghddhg", Path("/sgfsgf"), PluginMode.DISABLED),
#                         ("112dgh3", Path("/52234523"), PluginMode.ENABLED),
#                         ("sdfgh", Path("/ag543ya"), PluginMode.DISABLED)])
#
# print(x.get("112dgh3") == ("112dgh3", Path("/52234523"), PluginMode.ENABLED))
# print(x[3] == ("237", Path("/dhgd"), PluginMode.DISABLED))
#
# n = 0
# for c in x:
#     print(c == x[n])
#     n += 1
#
# print(x.all(PluginMode.DISABLED) == [("763", Path("/dhgd"), PluginMode.DISABLED),
#                                      ("237", Path("/dhgd"), PluginMode.DISABLED),
#                                      ("ghddhg", Path("/sgfsgf"), PluginMode.DISABLED),
#                                      ("sdfgh", Path("/ag543ya"), PluginMode.DISABLED)])
#
# print(x.ids() == ["1123","5465","763", "237", "11hgd23", "ghddhg", "112dgh3", "sdfgh"])
#
# print(x.ids(PluginMode.ENABLED) == ["1123", "5465", "11hgd23", "112dgh3"])
#
# print(x.package() == [Path("/"), Path("/6dghdg"), Path("/dhgd"), Path("/dhgd"), Path("/dgghstgdsh"), Path("/sgfsgf"),Path("/52234523"), Path("/ag543ya")])
#
# print(x.package(PluginMode.ENABLED) == [Path("/"), Path("/6dghdg"), Path("/dgghstgdsh"), Path("/52234523")])
# print(x.package(PluginMode.ENABLED))
# print([Path("/"), Path("/6dghdg"), Path("/dgghstgdsh"), Path("/52234523")])
