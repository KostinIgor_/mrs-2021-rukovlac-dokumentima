# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs2021rukovlacDokumentima-EngineBase -> model_I.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 12/12/2021 14:58
# @Desc   : Interface for plugins config model.
# '''=================================================
from abc import ABC, abstractmethod
from pathlib import Path
from typing import Iterator

from multimethod import multimethod

from app.plugin_framework.plugin.state import PluginMode

info = tuple[str, Path, PluginMode]


class PluginConfigList(ABC):
    @abstractmethod
    def __iter__(self) -> Iterator[info]: raise NotImplementedError

    @abstractmethod
    def __getitem__(self, item: int) -> info: raise NotImplementedError

    @abstractmethod
    def get(self, plugin_id: str) -> info:
        """
        Returns configuration info of plugin which id is provided, if plugin is not found None is returned,
        both enabled and disabled plugins will be taken in consideration

        :param plugin_id: Id of requested plugin
        """
        raise NotImplementedError

    @abstractmethod
    def all(self, mode: PluginMode) -> list[info]:
        """
        Returns configuration info of plugins in provided mode

        :param mode:
        """
        raise NotImplementedError

    @abstractmethod
    @multimethod
    def ids(self) -> list[str]:
        """
        Returns list of plugin ids that are in configuration
        """
        raise NotImplementedError

    @abstractmethod
    @multimethod
    def ids(self, mode: PluginMode) -> list[str]:
        """
        Returns list of plugin ids from configuration that are in given mode.

        :param mode:
        """
        raise NotImplementedError

    @abstractmethod
    @multimethod
    def package(self) -> list[Path]:
        """
        Returns a list of plugin's package for all plugins in configuration
        """
        raise NotImplementedError

    @abstractmethod
    @multimethod
    def package(self, mode: PluginMode) -> list[Path]:
        """
        Returns a list of plugin's package for all plugins in configuration, in given mode
        """
        raise NotImplementedError
