# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentima -> LoadedPlugins.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 22/12/2021 21:42
# @Desc   :
# '''=================================================
from abc import abstractmethod, ABC


class LoadedPlugins(ABC):

    @abstractmethod
    def __iter__(self): raise NotImplementedError

    @abstractmethod
    def __getitem__(self, item): raise NotImplementedError

    @abstractmethod
    def get(self, plugin_id: str): raise NotImplementedError

    @abstractmethod
    def get_by_extension_point(self, extension_point_id: int): raise NotImplementedError
