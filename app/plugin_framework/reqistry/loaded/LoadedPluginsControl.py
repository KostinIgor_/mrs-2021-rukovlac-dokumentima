# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentima -> LoadedPluginsControl.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 22/12/2021 21:41
# @Desc   :
# '''=================================================
from app.plugin_framework.plugin.plugin import Plugin
from app.plugin_framework.reqistry.loaded.LoadedPluginsModel import LoadedPluginsModel


class LoadedPluginsControl:
    def __init__(self, model: LoadedPluginsModel = None):
        self.model = model if model is not None else LoadedPluginsModel()

    def add(self, plugins: list[Plugin]):
        for plugin in plugins:
            self.model.plugins.append(plugin)

    def remove(self, plugin):
        self.model.plugins.remove(plugin)

    def pop(self, index):
        self.model.plugins.pop(index)
