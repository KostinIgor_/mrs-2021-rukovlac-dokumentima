# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentima -> LoadedPluginsModel.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 22/12/2021 21:42
# @Desc   :
# '''=================================================
from abc import ABC

from app.plugin_framework.plugin.plugin import Plugin
from app.plugin_framework.reqistry.loaded.LoadedPlugins import LoadedPlugins


class LoadedPluginsModel(LoadedPlugins, ABC):
    def __init__(self, plugins: list[Plugin] = None):
        self.plugins = plugins if plugins is not None else []

    def __iter__(self):
        for p in self.plugins:
            yield p

    def __getitem__(self, item):
        return self.plugins[item]

    def get(self, plugin_id):
        for p in self.plugins:
            if p.id == plugin_id:
                return p

    def get_by_extension_point(self, extension_point_id: int):
        plugins = []
        for p in self.plugins:
            if int(p.extension_point.id) == extension_point_id:
                plugins.append(p)

        return plugins
