# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs2021rukovlacDokumentima-EngineBase -> default_plugin_loader.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 12/12/2021 00:51
# @Desc   : Default plugin loader
# '''=================================================
import importlib
import inspect
import json
import os
from abc import ABC
from pathlib import Path

from multimethod import multimethod

from app.plugin_framework.plugin.spec.plugin_specification import PluginSpecification
from app.plugin_framework.plugin.state import PluginMode
from app.plugin_framework.reqistry.config.model_I import PluginConfigList
from app.plugin_framework.reqistry.loader.plugin_loader import PluginLoader


class DefaultPluginLoader(PluginLoader, ABC):
    DEFAULT_PLUGINS_PATH = Path("app/plugins/install")

    def __init__(self, config: PluginConfigList, path: str = None):
        self.config = config
        self.path = path if path is not None else self.DEFAULT_PLUGINS_PATH
        self.map = []

    @multimethod
    def load(self):
        plugins = []
        for package in [x for x in self.config.package(PluginMode.ENABLED) if x.is_dir()]:
            plugins.append(self.load(package))
        return plugins

    @multimethod
    def load(self, plugin_id: str):
        if self.config.get(plugin_id) is not None:
            _, path, _ = self.config.get(plugin_id)
            return self.load(path)
        # TODO : pretraga lokalnog foldera za otvaranjem svih specifikacija koje nisu u konfiguraciji

    @multimethod
    def load(self, path: Path):
        plugin_path = path / "plugin.py"
        data = {}
        with open(path / "specification.json") as fp:
            data = json.load(fp)
        specification = PluginSpecification.create(data)
        specification.path = path

        # dinamicko ucitavanje modula
        plugin = importlib.import_module((plugin_path.__str__()).replace(os.path.sep, ".").rstrip(".py"))
        clsmembers = inspect.getmembers(plugin_path, inspect.isclass)

        if len(clsmembers) == 1:
            return plugin, specification
        else:
            raise IndexError("The plugin.py module must contain just one class!")

    def disabled(self):
        print("Not implemented yet")
