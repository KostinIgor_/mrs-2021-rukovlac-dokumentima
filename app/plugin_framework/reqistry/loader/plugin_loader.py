# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs2021rukovlacDokumentima-EngineBase -> plugin_loader.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 12/12/2021 00:51
# @Desc   :Interface for plugin loader
# '''=================================================

from abc import ABC, abstractmethod
from pathlib import Path
from types import ModuleType

from multimethod import multimethod

from app.plugin_framework.plugin.spec.plugin_specification import PluginSpecification

PluginSpecCombo = tuple[ModuleType, PluginSpecification]


class PluginLoader(ABC):

    @abstractmethod
    @multimethod
    def load(self) -> list[PluginSpecCombo]:
        """
        Method that loads all downloaded, and installed plugin that are enabled
        """
        raise NotImplementedError

    @abstractmethod
    @multimethod
    def load(self, plugin_id: str) -> PluginSpecCombo:
        """
        A method that loads a specific (plugin_id) plugin, which is subsequently installed or enabled

        *The plugin_id will be searched first in the PluginLoader's map, if it is found, it means that the plugin is
        already installed,but it is only disabled, so the responsibility for his loading will be forwarded to
        PluginLoader.load(path) methods.

        If it isn't found, then the plugin isn't installed, the directory with installed plugins will be searched first,
        and after the specification with the forwarded plugin_id is found, further loading of plugins will be forwarded to
        PluginLoader.load(path) methods.
        """
        raise NotImplementedError

    @abstractmethod
    @multimethod
    def load(self, path: Path) -> PluginSpecCombo:
        """
        A method that loads a specific plugin on provided path, which is subsequently installed or enabled
        * This method will
        """
        raise NotImplementedError

    @abstractmethod
    def disabled(self) -> PluginSpecCombo:
        """
        A method that loads all disabled plugins
        """
        raise NotImplementedError
