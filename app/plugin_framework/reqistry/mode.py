# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs2021rukovlacDokumentima-EngineBase -> mode.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 05/12/2021 16:24
# @Desc   : Different display modes ( states ) for plugin marketplace ( plugin registry,
#           and partially  plugin manager view )
# '''=================================================
from enum import Enum


class MODE(Enum):
    """
    Class MODE represents different display modes for plugin Marketplace.

    MODE.MARKETPLACE - Plugin marketplace will show all plugins both installed and available,
    but the advantage in listing plugins will have uninstalled plugins.

    MODE.INSTALLED - Plugin marketplace will show only installed plugins.
    """
    MARKETPLACE = 1
    INSTALLED = 2
