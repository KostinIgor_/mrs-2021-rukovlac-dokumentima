from pathlib import Path

from PySide2.QtWidgets import QMainWindow

from app.plugin_framework.reqistry.config.configuration import RegistryPluginsConfiguration
from app.plugin_framework.reqistry.config.model_I import PluginConfigList
from app.plugin_framework.reqistry.loaded.LoadedPluginsControl import LoadedPluginsControl
from app.plugin_framework.reqistry.loader.plugin_loader import PluginLoader
from app.plugin_framework.reqistry.repository.PluginRepository import PluginRepository


class PluginRegistry:
    def __init__(self,
                 path: Path,
                 iface: QMainWindow,
                 repository: PluginRepository,
                 plugins: LoadedPluginsControl,
                 config: RegistryPluginsConfiguration,
                 loader: PluginLoader = None):

        self.path = path
        self.iface = iface  # interface (main_window)
        self.repository = repository
        self.config = config
        self.loader = loader
        self._plugins = plugins

        self.init()

    def init(self):
        for p in self.loader.load():
            plugin, specification = p
            self._plugins.add([plugin.Plugin(specification, self.iface)])

    def activate(self, plugin):
        exists = self._check_existing_plugin(plugin.id)
        if not exists:
            # FIXME: nisu proverene zavisnosti
            self._plugins.add([plugin])

    def uninstall(self, plugin):

        # FIXME: sta se radi sa zavisnostima
        self.deactivate(plugin.id)
        self._plugins.remove(plugin)

    def _check_existing_plugin(self, _id: str) -> bool:
        """
        Checks if plugin with _id is already in plugin list.
        """
        for plugin in self._plugins:
            if plugin.id == _id:
                return True
        return False

    @property
    def config_model(self) -> PluginConfigList:
        return self.config.config
