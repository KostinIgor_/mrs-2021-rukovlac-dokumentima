# -*- coding: UTF-8 -*-
"""
@Project ：mrs-2021-rukovlac-dokumentima 
@File ：AbstractPluginRepository.py
@Author ：Igor Kostin ( 2019270544 ) - KostinIgor_ (gitlab)
@Date ：11/30/21 , 2:46 PM
"""
from abc import ABC, abstractmethod
from pathlib import Path

import dill

from app.plugin_framework.reqistry.repository.PluginRepository import PluginRepository


class AbstractPluginRepository(PluginRepository, ABC):
    """

    """
    REPOSITORY_CACHE_FOLDERS = Path('app/cache/plugin/repo')
    CACHE_FILE_FORMAT = '.pickle'

    def __init__(self):
        self.map = []
        self.active = False
        self.plugins = []

    def is_active(self):
        return self.active

    def write(self):
        return
        # TODO: error
        dill.dump((self.plugins, self.map), file=open(self._file_path(), "wb"))

    def load(self):
        return
        try:
            self.plugins, self.map = dill.load(open(self._file_path(), "rb"))
        except FileNotFoundError:
            # TODO - error - File not found error
            # TODO - Logger - log error
            pass

    def _file_path(self):
        return (AbstractPluginRepository.REPOSITORY_CACHE_FOLDERS / self.cache_name) \
            .with_suffix(AbstractPluginRepository.CACHE_FILE_FORMAT)

    @abstractmethod
    def cache_name(self) -> str:
        """
        Returns the path of the file in which the object will be saved with all specifications for plugin
        """
        raise NotImplemented
