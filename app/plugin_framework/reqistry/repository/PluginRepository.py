# -*- coding: UTF-8 -*-
"""
@Project ：mrs-2021-rukovlac-dokumentima 
@File ：PluginRepository.py
@Author ：Igor Kostin ( 2019270544 ) - KostinIgor_ (gitlab)
@Date ：11/30/21 , 2:40 PM
"""
from abc import abstractmethod, ABC
from typing import Union

from app.plugin_framework.plugin.spec.plugin_specification import PluginSpecification
from app.plugin_framework.reqistry.mode import MODE


class ExtensionPoint:
    pass


class PluginRepository(ABC):
    """
    This is the interface for eny plugin repository, either for compound or concrete plugins repository, which provides
    all necessary functionality.
    """

    @abstractmethod
    def search(self, mode: MODE, parm: Union[str, ExtensionPoint] = None) -> list[
        PluginSpecification]: raise NotImplementedError

    @abstractmethod
    def download(self, plugin_id: str) -> bool:
        """
        Method that download/move the plugin and place it int the app/plugins/download dictionary.
        The concrete plugin repository will implement the download functionality, and it should be
        described in more details there.

        :param plugin_id: Id of plugin that will be moved into dictionary download.
        """
        raise NotImplementedError

    @abstractmethod
    def refresh(self) -> bool: raise NotImplemented

    @abstractmethod
    def load(self) -> bool: raise NotImplementedError

    @abstractmethod
    def write(self) -> bool:
        """
        Writes whole AbstractPluginRepository.plugins variable ( as stream using dill ) into local file system,
        for future use, for reasons of improvement applications responsiveness.
        Before writing into file system on each object should be applied Lite.relief() method in in purpose
        of object  size reduction.
        """
        raise NotImplementedError
