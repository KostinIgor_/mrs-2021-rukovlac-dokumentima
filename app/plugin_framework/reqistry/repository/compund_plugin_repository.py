from abc import ABC

from app.plugin_framework.reqistry.repository.AbstractPluginRepository import AbstractPluginRepository
from app.plugin_framework.reqistry.repository.PluginRepository import PluginRepository


class PluginDownloadError:
    pass


class PluginRepositoriesNotAvailable:
    pass


class CompoundPluginRepository(PluginRepository, ABC):
    def __init__(self, repositories: list[AbstractPluginRepository] = None):
        self.repositories = repositories if repositories is not None else []
        self.map = []  # TODO - loklana mapa za povezivanje plagina sa njihovim repozotorijumom

    # def get_primary_repo(self):
    #     # TODO: system-level-_settings : Dobaviti iz sistemskih podesavnja koji je repo selektovan kao primarni
    #     return 0

    def add(self, repository: AbstractPluginRepository):
        """
        Adds new repository at end of repositories list

        :param repository: New repository
        :return:
        """
        if repository in self.repositories:
            # TODO - razmisliti da li ima smisla uraditi nesto drugo u slucaju da je repozotorijum ve u list
            return True
        self.repositories.append(repository)

    def search(self, mode, parm=None):
        return sum([repo.search(mode, parm) for repo in self.repositories if repo.is_active()], [])

    def download(self, plugin_id):
        try:
            if len(self.repositories) <= 0:
                raise PluginRepositoriesNotAvailable

            for repo in [r for r in self.repositories if r.is_active()]:
                if repo.download(plugin_id):
                    return True
            raise PluginDownloadError
        except PluginRepositoriesNotAvailable:
            # TODO: error : prikazati korisniku prozor sa ispisanim detaljima o grsci,
            return False
        except PluginDownloadError:
            # TODO: error : prikazati korisniku prozor sa ispisanim detaljima o grsci,
            return False

    def refresh(self):
        for repo in self.repositories:
            repo.refresh()

    def load(self):
        for repo in self.repositories:
            repo.load()

    def write(self):
        for repo in self.repositories:
            repo.write()
