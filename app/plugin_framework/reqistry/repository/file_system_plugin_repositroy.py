# -*- coding: UTF-8 -*-
"""
@Project ：mrs-2021-rukovlac-dokumentima 
@File ：file_system_plugin_repositroy.py
@Author ：Igor Kostin ( 2019270544 ) - KostinIgor_ (gitlab)
@Date ：11/30/21 , 3:20 PM
"""
import json
from abc import ABC
from pathlib import Path
from typing import Union

from app.plugin_framework.plugin.spec.plugin_specification import PluginSpecification
from app.plugin_framework.reqistry.repository.AbstractPluginRepository import AbstractPluginRepository


class FileSystemPluginRepository(AbstractPluginRepository, ABC):
    DEFAULT_REPOSITORY_PATH = Path('app/plugins/download')

    def __init__(self, path: Union[str, Path] = None):
        super().__init__()
        self.path = (Path(path) if type(path) is str else path) if path is not None else self.DEFAULT_REPOSITORY_PATH

    def download(self, plugin_id):
        # In this case plugin is already downloaded and available locally, so it could just be installed,
        # but for application to work without bringing another complexity or another checkup for plugin ( is it
        # downloaded ) , this method will return true, so the plugin registry  can continue with installation of
        # plugin

        # TODO - fix - smisliti bolje resnje za ovo
        return True

    def refresh(self):
        """
        Gets specifications for all available plugins in local repository
        """
        if not self.path.exists():
            # TODO: pitati koriska da podesi novi folder za lokalne repozotorijume, bilo to da kreiranje novog
            #  foldera ili selekovanje postojacecg
            print(f'FOLDER {self.path} Ne postoji')
            return

        if not self.path.is_dir():
            # TODO: pitati koriska da podesi novi folder za lokalne repozotorijume, bilo to da kreiranje novog
            #  foldera ili selekovanje postojacecg
            print(f'Na putanji :  {self.path} se ne nalazi folder vec fajl.')
            return

        try:
            for plugin_dir in [x for x in self.path.iterdir() if x.is_dir()]:
                with open(plugin_dir / PluginSpecification.DEFAULT_SPECIFICATION_NAME) as spc_j:
                    data = json.load(spc_j)
                    specification = PluginSpecification.create(data)
                    self.plugins.append(specification)
                    self.map.append(
                        (specification.id, specification.extension_point.id, len(self.plugins) - 1, plugin_dir))
        except FileNotFoundError:
            # TODO: error : specification file not found, ne treba prekinut niti
            #  prikazivati korisniku ista, vec samo nastavviti sa izvrsavanjem  progrma
            pass
        except ValueError:
            # TODO: error : prikazati korisniku prozor sa ispisanim detaljima o grsci, kao i pluginom koji je
            #  nevalidan, kao i kljuceve koji nedostaju, ili bilo koja druga greska da je u pitanju
            # TODO: - fix - podizanje greske preilikom kreiranjaa spec
            pass
        finally:
            if len(self.plugins) > 0:
                self.active = True

    @property
    def cache_name(self):
        return "FileSystemPluginRepositorySpecCache"

    def search(self, mode, parm=None):
        return self.plugins
