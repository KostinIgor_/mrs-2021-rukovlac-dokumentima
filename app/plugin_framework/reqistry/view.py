# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs2021rukovlacDokumentima-EngineBase -> view.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 05/12/2021 14:54
# @Desc   : View for plugin registry
# '''=================================================

from PySide2.QtCore import Qt
from PySide2.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QStackedWidget, QPushButton, QSplitter, QLabel, \
    QScrollArea

from app.plugin_framework.reqistry.mode import MODE
from app.plugin_framework.reqistry.repository.PluginRepository import PluginRepository


class PluginMarketplace(QWidget):
    def __init__(self, parent, repository: PluginRepository):
        super().__init__(parent, Qt.Window)
        self.repository = repository
        self._mode = MODE.MARKETPLACE

        self.resize(980, 680)

        # MAIN COMPONENTS
        self.main_layout = QVBoxLayout()  # _main layout that contain all other layouts
        self.btn_layout = QHBoxLayout()  # top layout that contains buttons for different viewing mode
        self.main_container = QSplitter(Qt.Horizontal, self)  # splitter container for plugin list and stack
        self.plugin_list_layout = QVBoxLayout()  # list ( layout ) of all plugins ( filtered or not ), in vertical order
        self.plugin_stack = QStackedWidget()  # stack that  contains plugin specification view,
        # and also _settings( view ) for plugin that are installed, both are equally store on stack and on request are
        # shown. At first stack is empty, on opening each new plugin specification( view ), it ( view ) is kept on
        # stack and saved for future use, each plugin specification will get reference to to its view so it could
        # easily request from stack to show his view

        # BUTTONS SETUP
        # Button for selecting all plugins,advantage will have available plugins that are not installed yet,
        # but also there will be plugins that are already installed
        self.btn_marketplace = QPushButton("&Marketplace", self)
        self.btn_marketplace.clicked.connect(lambda: self.setMode(MODE.MARKETPLACE))

        # Button for filtering only plugins that are installed
        self.btn_installed = QPushButton("&Installed", self)
        self.btn_installed.clicked.connect(lambda: self.setMode(MODE.INSTALLED))

        self.btn_layout.addWidget(self.btn_marketplace)
        self.btn_layout.addWidget(self.btn_installed)

        # PLUGIN LIST SETUP
        for plugin_spec in self.repository.search(self.mode):
            self.plugin_list_layout.addWidget(
                plugin_spec.overview(self))  # For each plugin_spec method show that return
            # clickable widget with short overview for that plugin

        self.plugin_list_layout.addStretch(1)
        self.plugin_list_layout.setMargin(0)

        plugin_list_scroll_area = QScrollArea(self)
        #plugin_list_scroll_area.setStyleSheet("background-color: purple;")
        plugin_list_scroll_area.setWidgetResizable(True)

        plugin_list_scroll_content = QWidget(self)  # Container for plugin list layout
        plugin_list_scroll_content.setLayout(self.plugin_list_layout)
        #plugin_list_scroll_content.setStyleSheet("background-color: black")

        plugin_list_scroll_area.setWidget(plugin_list_scroll_content)

        # PLUGIN STACK SETUP
        #self.plugin_stack.setStyleSheet("background-color: green;")
        self.plugin_stack.addWidget(QLabel("Some Label "))

        # SPLITTER SETUP
        # _main splitter ( layout )  for working with plugins
        self.main_container.addWidget(plugin_list_scroll_area)
        self.main_container.addWidget(self.plugin_stack)
        self.main_container.setSizes([400, 400])

        # MAIN LAYOUT SETUP
        self.main_layout.addLayout(self.btn_layout)
        self.main_layout.addWidget(self.main_container)

        self.setLayout(self.main_layout)

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, new_mode: MODE):
        self._mode = new_mode

        if self._mode == MODE.MARKETPLACE:
            self.btn_marketplace.setEnabled(False)
            self.btn_installed.setEnabled(True)
        else:
            self.btn_marketplace.setEnabled(True)
            self.btn_installed.setEnabled(False)
