# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentimaDocumentPrototype_withPySide2 -> subscriber.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 09/01/2022 17:52
# @Desc   :
# '''=================================================
class Subscriber:
    def __init__(self, event, callback):
        self.callback = callback
        self.event = event
