# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentimaDocumentPrototype_withPySide2 -> subscription.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 09/01/2022 17:53
# @Desc   :
# '''=================================================
from abc import abstractmethod

from app.plugin_framework.subscription.subscriber import Subscriber


class Subscription:
    def subscribe(self, subscriber: Subscriber):
        self._subscriptions.append(subscriber)

    def _emit(self, event, args):
        for s in [s for s in self._subscriptions if s.event == event]:
            s.callback(args)

    @property
    @abstractmethod
    def subscriptions(self):
        raise NotImplementedError
