from app.plugin_framework.plugin.extension import Extension

from .widget import TextEdit


class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        self.widget = TextEdit(iface.central_widget)
        print("INIT TEST")

    # FIXME: implementacija apstraktnih metoda
    def activate(self):
        print("Activated")
        self.iface.add_widget(self.widget)

    def deactivate(self):
        print("Deactivated")
        self.iface.remove_widget(self.widget)
        print(self.widget)
