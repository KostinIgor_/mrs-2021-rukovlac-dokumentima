from PySide2.QtCore import Qt
from PySide2.QtGui import *

from app.plugin_framework.plugin.extension import Extension
from .widget import Toolbox


class QStringList:
    pass


class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        self.widget = Toolbox(iface.central_widget)

    # FIXME: implementacija apstraktnih metoda
    def activate(self):
        print("Right test_widget activated!")
        self.iface.addDockWidget(Qt.RightDockWidgetArea, self.widget)
        self.widget.show()

    def deactivate(self):
        print("Right test_widget deactivated!")
        self.iface.removeDockWidget(self.widget)
