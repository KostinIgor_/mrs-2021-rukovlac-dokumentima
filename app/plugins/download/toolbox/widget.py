from PySide2 import QtWidgets
from PySide2.QtGui import Qt


class Toolbox(QtWidgets.QDockWidget):
    widget_for = 6982346934

    def __init__(self, parent):
        super().__init__(parent)
        self.setAllowedAreas(Qt.RightDockWidgetArea or Qt.LeftDockWidgetArea)
        self.text_edit = QtWidgets.QTextEdit(self)
        self.text_edit.setText("This is example of toolbox widget")
        self.setWidget(self.text_edit)
