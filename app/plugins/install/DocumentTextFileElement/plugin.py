# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentima -> plugin.py.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 17/12/2021 18:10
# @Desc   :
# '''=================================================
from functools import reduce
from typing import Union

from PySide2 import QtWidgets
from PySide2.QtCore import QMimeData, QByteArray, QDir
from PySide2.QtGui import QIcon, QPainter, QColor, QDrag, Qt
from PySide2.QtWidgets import QToolBar, QWidget, QPushButton, QGraphicsTextItem, QColorDialog

from app.plugin_framework.plugin.extension import Extension
from app.plugins.install.document.panel import Settings
from documentLoader.serializable.q_graphics_text_item import QGraphicsTextItemSerialization
from documentLoader.serializable.serializable import Serializable


class TextFileSettings(Settings):
    def __init__(self, text_file, parent=None):
        super().__init__(parent, "TextFile Settings")
        self.text_file = text_file

        self.setBackgroundBrushColur = QPushButton("Set background color")
        self.setBackgroundBrushColur.clicked.connect(lambda: self.set_background_color())

        self.settings_layout.addWidget(self.setBackgroundBrushColur)

        self._settings.hide()

    def set_background_color(self):
        color = QColorDialog(self._main).getColor()
        if color.isValid():
            self.text_file.setDefaultTextColor(color)


class TextFIle(QGraphicsTextItem, QGraphicsTextItemSerialization):
    REQUIRED_KEYS = [
        "id",
        "file_path"
    ]

    def __init__(self, file_path=None):
        super().__init__()
        self.file_path = file_path
        print()
        self.open_dialog = None
        self._settings = None

        self.setTextInteractionFlags(Qt.TextEditorInteraction)
        self.open()

    def setRect(self, x, y, w, h):
        self.setPos(x, y)
        self.setTextWidth(w)

    @property
    def settings(self):
        if self._settings is None:
            self._settings = TextFileSettings(self)
        return self._settings

    def serialize(self):
        super_dict = super().serialize()
        this_dict = {
            "id":        "46786786376",
            "file_path": self.file_path
        }

        with open(self.file_path, 'w+') as f:
            f.write(self.toPlainText())

        return reduce(Serializable.update, (super_dict, this_dict), {})

    @staticmethod
    def deserialize(raw):
        if TextFIle.validate(raw):
            text = TextFIle(raw["file_path"])
        else:
            text = TextFIle()
        text._deserialize(raw)

        return text

    def _deserialize(self, raw):
        super().deserialize(raw)
        self.open(raw["file_path"])

    def open(self, path: [bool, str] = None):
        if path is None and self.file_path is None:
            dialog = QtWidgets.QFileDialog()
            dialog.setWindowTitle("Select file")
            dialog.setNameFilters(["*.txt"])
            dialog.setDirectory(QDir.currentPath())
            dialog.setFileMode(QtWidgets.QFileDialog.ExistingFiles)
            dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptOpen)

            if dialog.exec_() == QtWidgets.QDialog.Accepted:
                self.file_path = dialog.selectedFiles()[0]
            else:
                return
        elif self.file_path is None:
            self.file_path = path

        with open(self.file_path, 'r') as f:
            data = f.read()
            print(data)
            self.setPlainText(data)


class Plugin(Extension):
    def __init__(self, specification, iface):
        super().__init__(specification, iface)
        self.workspace = None
        self.pages = []
        self.manager = None
        print("Load plugin - DocumentTextFileElement")

    def init(self):
        print("Initialized plugin - DocumentSimpleTextElement")

    def activate(self, manager):
        print("Activated plugin - DocumentSimpleTextElement")

    def deactivate(self):
        print("Deactivated plugin - Workspace")

    def get_toolbar(self, parent: QToolBar):
        print((self.path / "assets/icon/icons8-document-48.png").__str__())
        action = ElementToolbarAction(QIcon((self.path / "assets/icon/icons8-document-48.png").__str__()),
                                      "Text file",
                                      parent,
                                      self.id,
                                      QByteArray())

        return [action]

    def element(self, parent):
        return TextFIle


class ElementToolbarAction(QPushButton):
    DEFAULT_EXTENSION_GROUP = "Document.Element"

    def __init__(self,
                 icon: Union[str, QIcon],
                 tooltip: str,
                 parent: Union[QWidget, None],
                 _id,
                 data: QByteArray):

        super().__init__(QIcon(icon) if type(icon) is str else icon,
                         "",
                         parent)

        self.data = data
        self.id = _id

        self.setToolTip(tooltip)

    def mouseMoveEvent(self, e):
        if e.button() == Qt.LeftButton:
            return

        mime_data = QMimeData()
        mime_data.setData(self.DEFAULT_EXTENSION_GROUP, self.data)
        mime_data.__setattr__("id", self.id)

        pixmap = QWidget.grab(self)  # Prika ikonice dok se element prevlaci

        # below makes the pixmap half transparent
        painter = QPainter(pixmap)
        painter.setCompositionMode(painter.CompositionMode_DestinationIn)
        painter.fillRect(pixmap.rect(), QColor(0, 0, 0, 127))
        painter.end()

        # make a QDrag
        drag = QDrag(self)
        # put our MimeData
        drag.setMimeData(mime_data)
        # set its Pixmap
        drag.setPixmap(pixmap)
        # shift the Pixmap so that it coincides with the cursor position
        drag.setHotSpot(e.pos())

        # start the drag operation
        # exec_ will return the accepted action from dropEvent
        if drag.exec_(Qt.DropAction or Qt.MoveAction) == Qt.MoveAction:
            self.clearFocus()
        else:
            self.clearFocus()
