# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentima -> plugin.py.py
# @IDE    :PyCharm
# @Author : Ivan Pecikoza - (https://gitlab.com/IvanPecikoza)
# @Time   : 15/1/2022 23:17
# @Desc   :
# '''=================================================
from pathlib import Path
from typing import Union

import PySide2
from PySide2.QtCore import QMimeData, QByteArray
from PySide2.QtGui import QIcon, QPainter, QColor, QDrag, Qt, QBrush, QTextOption, QFocusEvent
from PySide2.QtWidgets import QGraphicsView, QDialog, QToolBar, QWidget, QPushButton, QGraphicsSimpleTextItem, QGraphicsTextItem, QColorDialog

from app.plugin_framework.plugin.extension import Extension
from app.plugin_framework.subscription.subscription import Subscription
from app.plugins.install.document.panel import Settings

class LayoutSelector(QWidget):
    grview = QGraphicsView()


class Dialog(QDialog):
    def __init__(self, widget, parent = None):
        grview1 = QGraphicsView()
        grview2 = QGraphicsView()
        grview3 = QGraphicsView()
        layout = QGraphicsLinearLayout()
        scene1 = Template1()
        grview1.scene(scene1)
        grview1.scale(0.3,0.3)
        scene2 = Template2()
        grview2.scene(scene2)
        grview2.scale(0.3, 0.3)
        scene3 = Template3()
        grview3.scene(scene3)
        grview3.scale(0.3, 0.3)
        layout.addWidget(grview1)
        layout.addWidget(grview2)
        layout.addWidget(grview3)
        #scene1.setSceneRect(0, 0, 1800, 800)
        #scene1.setSceneRect(0, 0, 1800, 800)
        #scene1.setSceneRect(0, 0, 1800, 800)
        #grview.fitInView(scene1.sceneRect(), Qt.KeepAspectRatio)
        self.setLayout(layout)

    def MouseClickEvent(self):

class SimpleTextSettings(Settings):
    def __init__(self, simple_text, parent=None):
        super().__init__(parent, "SimpleText Settings")
        self.simple_text = simple_text

        self.setBackgroundBrushColur = QPushButton("Set background color")
        self.setBackgroundBrushColur.clicked.connect(lambda: self.set_background_color())

        self.settings_layout.addWidget(self.setBackgroundBrushColur)

        self._settings.hide()

    def set_background_color(self):
        color = QColorDialog(self._main).getColor()
        if color.isValid():
            self.simple_text.setDefaultTextColor(color)


class SimpleText(QGraphicsTextItem):
    def __init__(self, text="This is empty simple text"):
        super().__init__(text)
        self._settings = None


    def setRect(self, x, y, w, h):
        self.setPos(x, y)
        self.setTextWidth(w)

    @property
    def settings(self):
        if self._settings is None:
            self._settings = SimpleTextSettings(self)
        return self._settings


class Plugin(Extension):
    def __init__(self, specification, iface):
        super().__init__(specification, iface)
        self.workspace = None
        self.pages = []
        self.manager = None
        print("Load plugin - DocumentSimpleTextElement")

    def init(self):
        print("Initialized plugin - DocumentSimpleTextElement")

    def activate(self, manager):
        print("Activated plugin - DocumentSimpleTextElement")

    def deactivate(self):
        print("Deactivated plugin - Workspace")

    def get_toolbar(self, parent: QToolBar):
        action = ElementToolbarAction(QIcon((self.path / "assets/icon/icons8-test-tube-32.png").__str__()),
                                      "Simple text",
                                      parent,
                                      self.extension_point.id,
                                      QByteArray())

        return [action]

    def element(self, parent):
        return SimpleText("This is some simple text")


class ElementToolbarAction(QPushButton):
    #call dialog?
    dialog = Dialog()
    if dialog.exec_():
        pass

def __init__(self,
                 icon: Union[str, QIcon],
                 tooltip: str,
                 parent: Union[QWidget, None],
                 extension_point,
                 data: QByteArray):

        super().__init__(QIcon(icon) if type(icon) is str else icon,
                         "",
                         parent)

        self.data = data
        self.extension_point = extension_point

        self.setToolTip(tooltip)
        #self.connect(dialog)

    def mouseMoveEvent(self, e):
        if e.button() == Qt.LeftButton:
            return

        mime_data = QMimeData()
        mime_data.setData(self.DEFAULT_EXTENSION_GROUP, self.data)
        mime_data.__setattr__("extension_point", self.extension_point)

        pixmap = QWidget.grab(self)  # Prika ikonice dok se element prevlaci

        # below makes the pixmap half transparent
        painter = QPainter(pixmap)
        painter.setCompositionMode(painter.CompositionMode_DestinationIn)
        painter.fillRect(pixmap.rect(), QColor(0, 0, 0, 127))
        painter.end()

        # make a QDrag
        drag = QDrag(self)
        # put our MimeData
        drag.setMimeData(mime_data)
        # set its Pixmap
        drag.setPixmap(pixmap)
        # shift the Pixmap so that it coincides with the cursor position
        drag.setHotSpot(e.pos())

        # start the drag operation
        # exec_ will return the accepted action from dropEvent
        if drag.exec_(Qt.DropAction or Qt.MoveAction) == Qt.MoveAction:
            print('moved')
        else:
            print('copied')
