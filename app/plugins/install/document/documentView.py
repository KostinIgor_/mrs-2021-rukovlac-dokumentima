# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentimaDocumentPrototype -> documentView.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 07/01/2022 16:01
# @Desc   :
# '''=================================================
import random

from PySide2.QtGui import Qt
from PySide2.QtWidgets import QDockWidget, QWidget, QVBoxLayout, QHBoxLayout, QScrollArea, QGraphicsScene

from app.plugins.install.document.scalable import ScenePreview


class DocumentView(QDockWidget):
    def __init__(self, parent, pages, callback):
        super().__init__(parent)
        self.callback = callback

        self.setAllowedAreas(Qt.RightDockWidgetArea | Qt.LeftDockWidgetArea)
        self.setMaximumWidth(500)
        self.setMinimumWidth(200)
        self.main = QWidget(self)

        self.v_layout = QVBoxLayout()
        self.main_layout = QHBoxLayout()

        plugin_list_scroll_area = QScrollArea(self)
        plugin_list_scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        #plugin_list_scroll_area.setStyleSheet("background-color: black;")
        plugin_list_scroll_area.setWidgetResizable(True)
        self.scroll_area = plugin_list_scroll_area

        for p in pages:
            self._add(p)

        self.v_layout.addStretch(1)
        self.v_layout.setMargin(15)

        plugin_list_scroll_content = QWidget(self)  # Container for plugin list layout
        plugin_list_scroll_content.setLayout(self.v_layout)
        #plugin_list_scroll_content.setStyleSheet("background-color: red;")
        plugin_list_scroll_area.setWidget(plugin_list_scroll_content)

        self.main_layout.addWidget(plugin_list_scroll_area)
        self.main.setLayout(self.main_layout)

        self.setWidget(self.main)

        self._index = 0

    @property
    def index(self):
        return self._index

    @index.setter
    def index(self, val):
        self._index = val
        self.scroll_area.verticalScrollBar().rangeChanged.connect(self._scroll_to)
        self.focus()

    def focus(self):
        pass
        # for index in range(0, self.v_layout.count()-1):
        #     if index == self.index:
        #         self.v_layout.itemAt(self.index).widget().focus()
        #     else:
        #         self.v_layout.itemAt(index).widget().remove_focus()

    def add(self, new_scene: QGraphicsScene, index: int = -1):
        self._add(new_scene, index)
        self.index = index

    def _scroll_to(self):
        scroll = self.scroll_area.verticalScrollBar().minimum()
        if self.index == -1:
            scroll = self.scroll_area.verticalScrollBar().maximum()
        else:
            content_h = self.scroll_area.widget().height()
            content_count = self.v_layout.count()
            sh = content_h / content_count
            scroll = sh * (self.index - 1)
            print(content_h, content_count, sh, scroll, self.index)
        self.scroll_area.verticalScrollBar().setValue(scroll)
        self.scroll_area.verticalScrollBar().rangeChanged.disconnect(self._scroll_to)

    def _add(self, new_scene: QGraphicsScene, index: int = -1):
        if index == -1:
            index = self.v_layout.count() - 1
        self.v_layout.insertWidget(index, Container(self.main, new_scene, index, self.callback))

    def remove(self, index: int):
        print(f"Index je : {index}")
        widgetToRemove = self.v_layout.itemAt(index).widget()

        widgetToRemove.hide()
        self.v_layout.removeWidget(widgetToRemove)
        widgetToRemove.setParent(None)

    # def clear(self):
    #     for index in range(self.v_layout.count()-1):
    #         widgetToRemove = self.v_layout.itemAt(index).widget()
    #
    #         widgetToRemove.hide()
    #         self.v_layout.removeWidget(widgetToRemove)
    #         widgetToRemove.setParent(None)


class Container(QWidget):
    def __init__(self, parent, scene, index, _callback):
        super().__init__(parent)
        main_layout = QHBoxLayout()
        main_layout.setMargin(0)

        scene_wrap = QWidget(self)

        # print(f"Main width - {self.width()}, {scene_wrap.width()}")
        self.preview = ScenePreview(scene, scene_wrap, _callback, 0.98)
        self.preview.adjust(parent.parent().width() if scene_wrap.width() < 200 else scene_wrap.width())

        scene_layout = QHBoxLayout()
        scene_layout.setMargin(0)
        scene_layout.addWidget(self.preview)

        scene_wrap.setLayout(scene_layout)

        self.additional = QWidget(self)
        self.additional.setStyleSheet("background:blue;")
        self.additional.setFixedWidth(5)

        main_layout.addWidget(scene_wrap)
        main_layout.addWidget(self.additional)
        self.setLayout(main_layout)

    def focus(self):
        self.additional.setStyleSheet(
            f"background:rgb({random.randint(0, 255)},{random.randint(0, 255)},{random.randint(0, 255)});")

    def remove_focus(self):
        self.additional.setStyleSheet("background:blue;")
