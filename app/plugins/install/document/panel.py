# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentimaDocumentPrototype -> panel.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 07/01/2022 16:01
# @Desc   :
# '''=================================================

from PySide2.QtGui import QBrush, QFocusEvent, QIcon, QColor
from PySide2.QtWidgets import QGraphicsView, QWidget, QVBoxLayout, QPushButton, QColorDialog

from app.plugin_framework.subscription.subscriber import Subscriber
from app.plugin_framework.subscription.subscription import Subscription


class Settings:
    def __init__(self, parent, title):
        self._main = QWidget(parent)
        self.shown = False
        self.open_i = QIcon("app/assets/icon/icon8/Fluent/icons8-sort-up-48.png")
        self.closed_i = QIcon("app/assets/icon/icon8/Fluent/icons8-sort-down-48.png")

        #self._main.setStyleSheet("background-color:yellow;")

        self._main_layout = QVBoxLayout()
        self._main_layout.setMargin(0)

        self._settings = QWidget()
        self.settings_layout = QVBoxLayout()
        self._settings.setLayout(self.settings_layout)

        self._title = QPushButton(self.closed_i, title)
        self._title.setStyleSheet("Text-align:left")
        self._title.clicked.connect(self.expand)

        self._main_layout.addWidget(self._title)
        self._main_layout.addWidget(self._settings)

        self._main.setLayout(self._main_layout)

    def get(self):
        return self._main

    def expand(self):
        if self.shown:
            self.shown = False
            self._settings.hide()
            self._title.setIcon(self.closed_i)
        else:
            self.shown = True
            self._settings.show()
            self._title.setIcon(self.open_i)


class PanelSettings(Settings):
    def __init__(self, panel, parent=None):
        super().__init__(parent, "Panel Settings")
        self.panel = panel

        self.setBackgroundBrushColur = QPushButton("Set background color")
        self.setBackgroundBrushColur.clicked.connect(lambda: self.set_background_color())

        self.set_border_color_btn = QPushButton("Set border color")
        self.set_border_color_btn.clicked.connect(lambda: self.set_border_color())

        self.settings_layout.addWidget(self.setBackgroundBrushColur)
        self.settings_layout.addWidget(self.set_border_color_btn)

        self._settings.hide()

    def set_background_color(self):
        color = QColorDialog(self._main).getColor()
        if color.isValid():
            self.panel.scene.setBackgroundBrush(QBrush(color))

    def set_border_color(self):
        color = QColorDialog(self._main).getColor()
        print(color)
        if color.isValid():
            self.panel.scene.border_pen = QColor(4278190080)


class Panel(QGraphicsView, Subscription):
    def __init__(self, scene, parent: QWidget):
        super().__init__(scene, parent)
        self.scene = scene
        self._subscriptions = []

        self.settings = None

        self.width = scene.width()
        self.height = scene.height()

        self.parent = parent

        self.scene.subscribe(Subscriber("NewItem", self.new_item))
        for slot in [slot for slot in scene.items() if type(slot).__name__ == "Slot"]:
            slot.subscribe(Subscriber(QFocusEvent.FocusIn, self.slot_selected))

    def new_item(self, item):
        if type(item).__name__ == "Slot":
            item.subscribe(Subscriber(QFocusEvent.FocusIn, self.slot_selected))

    def focusInEvent(self, event: QFocusEvent) -> None:
        # print("PanelFocusInEvent")
        self._emit(QFocusEvent.FocusIn, [self.settings if self.settings is not None else self._gen_settings()])

    # def focusOutEvent(self, event: QFocusEvent) -> None:
    #     self._emit(QFocusEvent.FocusOut, None)

    def slot_selected(self, args):
        self._emit(QFocusEvent.FocusIn, [*args, self.settings if self.settings is not None else self._gen_settings()])

    def slot_uncelected(self, args):
        self._emit(QFocusEvent.FocusOut, [*args, self.settings])

    def _gen_settings(self):
        self.settings = PanelSettings(self)
        return self.settings
