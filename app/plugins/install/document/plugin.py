# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentima -> plugin.py.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 17/12/2021 18:10
# @Desc   :
# '''=================================================
import json

from PySide2 import QtWidgets
from PySide2.QtCore import QDir
from PySide2.QtGui import QPen, Qt, QFocusEvent, QIcon
from PySide2.QtWidgets import QWidget, QGraphicsView, QGraphicsScene, QHBoxLayout, QToolBar, QAction, QMessageBox
from multimethod import multimethod

from __mainTets import Scene
from app.plugin_framework.plugin.extension import Extension
from app.plugin_framework.subscription.subscriber import Subscriber
from app.plugins.install.document.documentView import DocumentView
from app.plugins.install.document.scalable import ScalableView
from app.plugins.install.document.settingsToolbox import SettingsView

style = None


class DumObj:
    def __init__(self):
        self.attribute1 = "attribute1"
        self.attribute2 = "attribute2"


class Plugin(Extension):
    def __init__(self, specification, iface):
        super().__init__(specification, iface)
        self.workspace = None

        self.tool_box = SettingsView(self.iface)
        self.doc_view = None
        self.tool_bar = QToolBar(self.iface.tool_bar)
        self.active_page = None

        print("Load plugin - Document")

    def init(self):
        # self.workspace = self.manager.get("Document.Workspace")
        # self.scene = self.manager.get("Document.Scene")
        # self.elements = [self.manager.get(782745872452), self.manager.get(46786786376) ]
        self.elements = self.manager.get_by_extension_point(4546516565)
        # self.workspace.setScene(self.scenes[0])

    def getWorkingPanel(self, parent) -> QWidget:
        view = QGraphicsView(parent)
        view.setStyleSheet(
            "background-color: grey;"
        )
        scene = QGraphicsScene(view)

        ellipse = scene.addEllipse(20, 20, 200, 200)

        view.setScene(scene)

        return view

    def activate(self, manager, pages=[], path=None):
        self.file_path = path
        self.manager = manager
        self.pages = pages
        self.tool_bar.addSeparator()
        new_a = QAction(QIcon("app/assets/icon/icon8/Fluent/icons8-new-slide-48.png"), 'New empty page', self.tool_bar)
        new_a.triggered.connect(self.add)

        save_a = QAction(QIcon("app/assets/icon/icon8/Fluent/icons8-save-32.png"), 'Save', self.tool_bar)
        save_a.triggered.connect(self.save)
        self.tool_bar.addAction(new_a)
        self.tool_bar.addAction(save_a)

        self.tool_bar.addSeparator()
        self.iface.tool_bar.addWidget(self.tool_bar)
        self.init()

        for el in self.elements:
            for toolbar in el.get_toolbar(self.tool_bar):
                self.tool_bar.addWidget(toolbar)

        # print(self.elements)
        pen = QPen(Qt.red)

        self.doc_view = DocumentView(self.iface, self.pages, self.open)
        self.iface.addDockWidget(Qt.LeftDockWidgetArea, self.doc_view)
        self.iface.addDockWidget(Qt.RightDockWidgetArea, self.tool_box)

        if len(self.pages) != 0:
            self.open(0)

        print("Activated plugin - Workspace")

    def save(self):
        if self.file_path is None:
            dialog = QtWidgets.QFileDialog()
            dialog.setWindowTitle("Save file")
            dialog.setNameFilters(["DOCXY (*.docxy)"])
            dialog.setDirectory(QDir.currentPath())
            dialog.setFileMode(QtWidgets.QFileDialog.AnyFile)
            dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptSave)

            if dialog.exec_() == QtWidgets.QDialog.Accepted:
                path = dialog.selectedFiles()[0]
                self.file_path = path + ".docxy"
            else:
                return

        stream = [scene.serialize() for scene in self.pages]

        with open(self.file_path, "w+") as file:
            json.dump(stream, file, indent=4)

    def save_as(self, path):
        if path is None:
            dialog = QtWidgets.QFileDialog()
            dialog.setWindowTitle("Save file")
            dialog.setNameFilters(["DOCXY (*.docxy)"])
            dialog.setDirectory(QDir.currentPath())
            dialog.setFileMode(QtWidgets.QFileDialog.AnyFile)
            dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptSave)

            if dialog.exec_() == QtWidgets.QDialog.Accepted:
                path = dialog.selectedFiles()[0]
                self.file_path = path + ".docxy"
            else:
                return
        self.save()

    @staticmethod
    def load(path):
        print("Loaded file at : ", path)
        with open(path, "r") as file:
            stream = json.load(file)
            return stream

    def add(self, new_scene: QGraphicsScene = None, index=-1):

        if new_scene is None:
            new_scene = Scene(1000, 800, self.manager)
        self.pages.append(new_scene)
        self.open(new_scene)
        self.doc_view.add(new_scene, index)

    def _add(self, new_scene: QGraphicsScene = None, index=-1):

        if new_scene is None:
            new_scene = Scene(1000, 800, self.manager)
        self.pages.insert(index, new_scene)
        self.doc_view.add(new_scene, index)

    def set(self, scenes, path):
        if len(self.pages) != 0:
            # do u want to save
            self.file_path = path

            self.clear()

            for s in scenes:
                self._add(s)
            if len(self.pages) > 0:
                self.open(0)
            return

        self.file_path = path
        for s in scenes:
            self.add(s)
        self.open(0)

    @multimethod
    def open(self, index: int):
        # print(f"Otvaram index : {index}")
        self.tool_box.clear()
        scene = self.pages[index]
        self.active_page = index

        wrap = QWidget()

        wrap_L = QHBoxLayout()
        wrap_L.setMargin(0)

        graphicView = ScalableView(scene, self.iface.central_widget)
        graphicView.subscribe(Subscriber(QFocusEvent.FocusIn, self.tool_box.setSettings))
        graphicView.adjust(self.iface.central_widget.width())

        wrap_L.addWidget(graphicView)

        wrap.setLayout(wrap_L)

        self.iface.add_widget(wrap)

    @multimethod
    def open(self, scene: QGraphicsScene):
        for i, _scene in enumerate(self.pages):
            if _scene == scene:
                self.open(i)
                return

    def show_settings(self, args):
        settings = args
        self.tool_box.setSettings(settings)

    def deactivate(self):
        print("Deactivated plugin - Workspace")

    def deactivate(self):
        print("Deactivated plugin - Workspace")

    def delete(self):
        if self.active_page is None:
            pass
        elif self.pages[self.active_page].focusItem() is not None:
            self.pages[self.active_page].removeItem(self.pages[self.active_page].focusItem())
        else:
            choice = QMessageBox.question(self.iface, 'Delete Page', 'Are u sure you want to delete this PAGE?',
                                          QMessageBox.Ok | QMessageBox.Cancel)

            if choice == QMessageBox.Ok:
                self.deletePage(self.active_page)
            else:
                pass

    def clear(self):
        for index in range(len(self.pages) - 1, -1, -1):
            self.pages.pop(index)
            self.doc_view.remove(index)

            if len(self.pages) == 0:
                self.iface.remove_widget()

    def deletePage(self, index: int):
        self.pages.pop(index)
        self.doc_view.remove(index)

        _index = self.active_page - 1
        if _index <= -1:
            _index = 0
        if len(self.pages) == 0:
            self.iface.remove_widget()
            return

        self.open(_index)
