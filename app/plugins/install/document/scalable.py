# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentimaDocumentPrototype -> scalable.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 07/01/2022 16:01
# @Desc   :
# '''=================================================
from PySide2.QtGui import QResizeEvent, Qt, QMouseEvent
from PySide2.QtWidgets import QWidget, QGraphicsScene

from app.plugins.install.document.panel import Panel


class ScalableView(Panel):
    def __init__(self, scene: QGraphicsScene, parent: QWidget, margin=0.95):
        super().__init__(scene, parent)
        scene.setParent(self)
        self.margin = margin

        self.parent.resizeEvent = self._scale

    def _scale(self, e: QResizeEvent):
        # print("Resized")
        if e.oldSize().width() != e.size().width():
            if self._scale_width(e.size().width()):
                return

        if e.oldSize().height() != e.size().height():
            if self._scale_height(e.size().height()):
                return

    def _scale_width(self, width: int):
        # print("Scale width")
        factor = width * 0.00099
        # print(self.height * factor, self.parent.height())
        if self.height * factor < self.parent.height():
            # print("Scale width - APPROVED")
            self.re_scale(factor)
            return True
        # print("Scale width - NOT APPROVED")
        return False

    def _scale_height(self, height: int):
        # print("Scale height")
        factor = height * 0.00124
        # print(f"{self.width * factor} < {self.parent.width()} - {self.width * factor < self.parent.width()}")
        # print(f"{self.height * factor} < {self.parent.height()} - {self.height * factor < self.parent.height()}")
        if self.width * factor < self.parent.width():
            # print("Scale height - APPROVED")
            self.re_scale(factor)
            return True
        # print("Scale height - NOT APPROVED")
        return False

    def re_scale(self, factor):
        self.resetMatrix()
        self.scale((factor * self.margin), (factor * self.margin))

    def adjust(self, width=None) -> None:
        self._scale_width(self.parent.width())
        self._scale_height(self.parent.height())


class PreviewScalableView(ScalableView):
    def __init__(self, scene: QGraphicsScene, parent: QWidget, margin=0.95):
        super().__init__(scene, parent, margin)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

    def _scale(self, e: QResizeEvent):
        # print("Resized")
        if e.oldSize().width() != e.size().width():
            self._scale_width(e.size().width())

    def _scale_width(self, width: int):
        factor = width * 0.001
        sh = self.height * factor
        # print(f"Scaled / factor = {factor} / sh = {sh}")
        # print(f"Width : {width}, Page width: {self.width * factor * self.margin}")
        self.parent.setFixedHeight(sh)

        self.re_scale(factor)
        return True

    def adjust(self, width=None) -> None:
        if width is None:
            width = self.parent.width()
        self._scale_width(width)


class ScenePreview(PreviewScalableView):
    def __init__(self, scene: QGraphicsScene, parent: QWidget, callback, margin=0.95):
        super().__init__(scene, parent, margin)
        self.callback = callback

    def mousePressEvent(self, event: QMouseEvent) -> None:
        self.callback(self.scene)

    def mouseDoubleClickEvent(self, event: QMouseEvent) -> None:
        return
