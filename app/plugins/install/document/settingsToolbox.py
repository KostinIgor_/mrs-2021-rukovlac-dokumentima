# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentimaDocumentPrototype -> settingsToolbox.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 07/01/2022 16:00
# @Desc   :
# '''=================================================
from PySide2.QtCore import Qt
from PySide2.QtWidgets import QDockWidget, QWidget, QVBoxLayout, QScrollArea


class SettingsView(QDockWidget):
    def __init__(self, parent):
        super().__init__(parent)

        self.setAllowedAreas(Qt.RightDockWidgetArea | Qt.LeftDockWidgetArea)

        self.main = QWidget(self)
        self.widgets = []
        self.map = []

        self.main_layout_wrap = QVBoxLayout()
        # self.main_layout_wrap.setMargin(0)
        self.main_layout = QVBoxLayout()
        self.stretch_layout = QVBoxLayout()
        self.stretch_layout.addStretch()

        scroll_wrap_layout = QVBoxLayout()
        # scroll_wrap_layout.setMargin(0)

        settings_scroll_area = QScrollArea(self)
        #settings_scroll_area.setStyleSheet("background-color: purple;")
        settings_scroll_area.setWidgetResizable(True)

        settings_scroll_content = QWidget(self)  # Container for plugin list layout
        settings_scroll_content.setLayout(self.main_layout_wrap)
        #settings_scroll_content.setStyleSheet("background-color: red")

        settings_scroll_area.setWidget(settings_scroll_content)

        scroll_wrap_layout.addWidget(settings_scroll_area)

        # Styling
        # self.main.setStyleSheet("QWidget{"
        #                         "background-color:blue;}")
        self.main.setFixedWidth(300)

        self.main.setLayout(scroll_wrap_layout)

        self.main_layout_wrap.addLayout(self.main_layout)
        self.main_layout_wrap.addLayout(self.stretch_layout)

        self.setWidget(self.main)

    def setSettings(self, settings: list):
        self.clear()
        settings = [_s for _s in settings if _s is not None]
        for s in settings:
            if s in self.widgets:
                self.main_layout.itemAt(self.widgets.index(s)).widget().show()
            else:
                self.widgets.append(s)
                self.main_layout.addWidget(s.get(), -1)

    def clear(self):
        for i in reversed(range(self.main_layout.count())):
            widgetToRemove = self.main_layout.itemAt(i).widget()
            widgetToRemove.hide()
            # remove it from the layout list
            # self.main_layout.removeWidget(widgetToRemove)
            # remove it from the gui
            # widgetToRemove.setParent(None)

    def hideSettings(self, settings):
        self.clear()
