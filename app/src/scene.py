# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentima -> scene.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 15/01/2022 16:50
# @Desc   :
# '''=================================================
from PySide2.QtWidgets import QGraphicsScene


class Scene(QGraphicsScene):
    def __init__(self, width, height, manager=None):
        super().__init__()

        self.manager = manager

        self.setSceneRect(0, 0, width, height)

        self._items = []

        # BORDER LINES
        self._border_pen = None

        # self._top_line = QLineF(self.sceneRect().topLeft(), self.sceneRect().topRight())
        # self.top_line = QGraphicsLineItem(self._top_line)
        #
        # self._right_line = QLineF(self.sceneRect().topRight(), self.sceneRect().bottomRight())
        # self.right_line = QGraphicsLineItem(self._right_line)
        #
        # self._bottom_line = QLineF(self.sceneRect().bottomRight(), self.sceneRect().bottomLeft())
        # self.bottom_line = QGraphicsLineItem(self._bottom_line)
        #
        # self._left_line = QLineF(self.sceneRect().topLeft(), self.sceneRect().bottomLeft())
        # self.left_line = QGraphicsLineItem(self._left_line)
        #
        # self.addItem(self.top_line)
        # self.addItem(self.right_line)
        # self.addItem(self.bottom_line)
        # self.addItem(self.left_line)

        self._items = []
