# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentima -> q_graphic_pixmap_item_serialization.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 20/01/2022 15:25
# @Desc   :
# '''=================================================
from functools import reduce

from documentLoader.serializable.q_graphics_item import QGraphicsItemSerialization
from documentLoader.serializable.serializable import Serializable


class QGraphicsPixmapItemSerialization(QGraphicsItemSerialization):
    REQUIRED_KEYS = [""]

    def serialize(self):
        super_dict = super().serialize()
        this_dict = {
        }

        return reduce(Serializable.update, (super_dict, this_dict), {})

    def deserialize(self, raw):
        super().deserialize(raw)
        if QGraphicsPixmapItemSerialization.validate(raw):
            pass
