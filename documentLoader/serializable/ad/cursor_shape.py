# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentima -> cursor_shape.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 17/01/2022 20:08
# @Desc   :
# '''=================================================
from typing import Union

from PySide2.QtCore import Qt
from PySide2.QtGui import QCursor


class CursorShape:
    shapes = [
        (Qt.CursorShape.ArrowCursor, 0),
        (Qt.CursorShape.UpArrowCursor, 1),
        (Qt.CursorShape.CrossCursor, 2),
        (Qt.CursorShape.WaitCursor, 3),
        (Qt.CursorShape.IBeamCursor, 4),
        (Qt.CursorShape.SizeVerCursor, 5),
        (Qt.CursorShape.SizeHorCursor, 6),
        (Qt.CursorShape.SizeBDiagCursor, 7),
        (Qt.CursorShape.SizeFDiagCursor, 8),
        (Qt.CursorShape.SizeAllCursor, 9),
        (Qt.CursorShape.BlankCursor, 10),
        (Qt.CursorShape.SplitVCursor, 11),
        (Qt.CursorShape.SplitHCursor, 12),
        (Qt.CursorShape.PointingHandCursor, 13),
        (Qt.CursorShape.ForbiddenCursor, 14),
        (Qt.CursorShape.OpenHandCursor, 17),
        (Qt.CursorShape.ClosedHandCursor, 18),
        (Qt.CursorShape.WhatsThisCursor, 15),
        (Qt.CursorShape.BusyCursor, 16),
        (Qt.CursorShape.DragMoveCursor, 20),
        (Qt.CursorShape.DragCopyCursor, 19),
        (Qt.CursorShape.DragLinkCursor, 21),
        (Qt.CursorShape.BitmapCursor, 24)
    ]

    @staticmethod
    def convert(code: int) -> Qt.CursorShape:
        for cursor, _code in CursorShape.shapes:
            if code == _code:
                return cursor
        return Qt.CursorShape.ArrowCursor

    @staticmethod
    def extract(cursor: Union[Qt.CursorShape, QCursor]) -> int:
        cursor = cursor if type(cursor).__name__ == 'Qt.CursorShape' else cursor.shape()
        for _cursor, code in CursorShape.shapes:
            if cursor == _cursor:
                return code
        return 0
