# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentima -> item_flags.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 17/01/2022 20:08
# @Desc   :
# '''=================================================
from PySide2.QtWidgets import QGraphicsItem


class ItemFlags:
    flags = [
        (QGraphicsItem.ItemIsMovable, "0x1"),
        (QGraphicsItem.ItemIsSelectable, "0x2"),
        (QGraphicsItem.ItemIsFocusable, "0x4"),
        (QGraphicsItem.ItemClipsToShape, "0x8"),
        (QGraphicsItem.ItemClipsChildrenToShape, "0x10"),
        (QGraphicsItem.ItemIgnoresTransformations, "0x20"),
        (QGraphicsItem.ItemIgnoresParentOpacity, "0x40"),
        (QGraphicsItem.ItemDoesntPropagateOpacityToChildren, "0x80"),
        (QGraphicsItem.ItemStacksBehindParent, "0x100"),
        (QGraphicsItem.ItemUsesExtendedStyleOption, "0x200"),
        (QGraphicsItem.ItemHasNoContents, "0x400"),
        (QGraphicsItem.ItemSendsGeometryChanges, "0x800"),
        (QGraphicsItem.ItemAcceptsInputMethod, "0x1000"),
        (QGraphicsItem.ItemNegativeZStacksBehindParent, "0x2000"),
        (QGraphicsItem.ItemIsPanel, "0x4000"),
        (QGraphicsItem.ItemSendsScenePositionChanges, "0x10000"),
        (QGraphicsItem.ItemContainsChildrenInShape, "0x80000"),
    ]

    @staticmethod
    def convert(codes: list[str]):
        flags = []

        for flag, code in ItemFlags.flags:
            if code in codes:
                flags.append(flag)

        return flags

    @staticmethod
    def extract(flags):
        extracted = []
        for flag, code in ItemFlags.flags:
            if flag & flags:
                extracted.append(code)

        return extracted
