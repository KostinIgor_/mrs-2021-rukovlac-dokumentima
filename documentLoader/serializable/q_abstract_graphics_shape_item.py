# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentima -> q_abstract_graphics_shape_item.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 17/01/2022 19:37
# @Desc   :
# '''=================================================
from functools import reduce

from PySide2.QtGui import QColor, QPen, QBrush

from documentLoader.serializable.q_graphics_item import QGraphicsItemSerialization
from documentLoader.serializable.serializable import Serializable


class QAbstractGraphicsShapeItemSerialization(QGraphicsItemSerialization):
    REQUIRED_KEYS = ["pen", "brush"]

    def serialize(self):
        super_dict = super().serialize()
        this_dict = {
            "pen":   self.pen().color().rgba(),
            "brush": self.brush().color().rgba()
        }

        return reduce(Serializable.update, (super_dict, this_dict), {})

    def deserialize(self, raw):
        super().deserialize(raw)
        if QAbstractGraphicsShapeItemSerialization.validate(raw):
            self.setPen(QPen(QColor().fromRgba(raw["pen"])))
            self.setBrush(QBrush(QColor().fromRgba(raw["brush"])))
