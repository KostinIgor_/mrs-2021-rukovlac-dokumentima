# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentima -> q_graphic_item.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 17/01/2022 10:40
# @Desc   :
# '''=================================================
from PySide2.QtGui import QTransform, QCursor

from documentLoader.serializable.ad.cursor_shape import CursorShape
from documentLoader.serializable.ad.item_flags import ItemFlags
from documentLoader.serializable.serializable import Serializable


class QGraphicsItemSerialization(Serializable):
    REQUIRED_KEYS = ["pos",
                     "scale",
                     "rotation",
                     "transform",
                     "transformOriginPoint",
                     "flags",
                     "isEnabled",
                     "isSelected",
                     "acceptDrops",
                     "acceptHoverEvents",
                     "acceptTouchEvents",
                     # "acceptedMouseButtons",
                     "filtersChildEvents",
                     "cursor",
                     # "inputMethodHints",
                     "opacity",
                     "boundingRegionGranularity",
                     "zValue",
                     # "childItems"]
                     ]

    def serialize(self):
        return {
            "pos":                       [self.pos().x(), self.pos().y()],
            "scale":                     self.scale(),
            "rotation":                  self.rotation(),
            "transform":                 [self.transform().m11(), self.transform().m12(), self.transform().m13(),
                                          self.transform().m21(), self.transform().m22(), self.transform().m23(),
                                          self.transform().m31(), self.transform().m32(), self.transform().m33()],
            "transformOriginPoint":      [self.transformOriginPoint().x(), self.transformOriginPoint().y()],
            "flags":                     ItemFlags.extract(self.flags()),
            "isEnabled":                 self.isEnabled(),
            "isSelected":                self.isSelected(),
            "acceptDrops":               self.acceptDrops(),
            "acceptHoverEvents":         self.acceptHoverEvents(),
            "acceptTouchEvents":         self.acceptTouchEvents(),
            # "acceptedMouseButtons":      self.acceptedMouseButtons(),  #
            "filtersChildEvents":        self.filtersChildEvents(),
            "cursor":                    CursorShape.extract(self.cursor()),
            # "inputMethodHints":          self.inputMethodHints(),  #
            "opacity":                   self.opacity(),
            "boundingRegionGranularity": self.boundingRegionGranularity(),
            "zValue":                    self.zValue(),
            # "childItems":                self.childItems(),  #
        }

    def deserialize(self, raw):
        if QGraphicsItemSerialization.validate(raw):
            self.setPos(*raw["pos"])
            self.setScale(raw["scale"])
            self.setRotation(raw["rotation"])
            self.setTransform(QTransform(*raw["transform"]))
            self.setTransformOriginPoint(*raw["transformOriginPoint"])
            for flag in ItemFlags.convert(raw["flags"]):
                self.setFlag(flag)
            self.setEnabled(raw["isEnabled"])
            self.setSelected(raw["isSelected"])
            self.setAcceptDrops(raw["acceptDrops"])
            self.setAcceptHoverEvents(raw["acceptHoverEvents"])
            self.setAcceptTouchEvents(raw["acceptTouchEvents"])
            # self.setAcceptedMouseButtons(raw["acceptedMouseButtons"])  #
            self.setFiltersChildEvents(raw["filtersChildEvents"])
            self.setCursor(QCursor(CursorShape.convert(raw["cursor"])))
            # self.setInputMethodHints(raw["inputMethodHints"])  #
            self.setOpacity(raw["opacity"])
            self.setBoundingRegionGranularity(raw["boundingRegionGranularity"])
            self.setZValue(raw["zValue"])
