# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentima -> q_graphics_object.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 18/01/2022 16:12
# @Desc   :
# '''=================================================
from functools import reduce

from documentLoader.serializable.q_graphics_item import QGraphicsItemSerialization
from documentLoader.serializable.q_object import QObjectSerialization
from documentLoader.serializable.serializable import Serializable


class QGraphicObjectSerialization(QObjectSerialization, QGraphicsItemSerialization):
    REQUIRED_KEYS = [
        # "effect",
        # "parent",
        "visible",
        "x",
        "y"]

    def serialize(self):
        super_dict_object = super().serialize()
        super_dict_item = super(QObjectSerialization, self).serialize()

        this_dict = {
            # "effect" : self.effect(),
            # "parent" : self.parent(),
            "visible": self.isVisible(),
            "x":       self.x(),
            "y":       self.y()
        }

        return reduce(Serializable.update, (super_dict_object, super_dict_item, this_dict), {})

    def deserialize(self, raw):
        super(QObjectSerialization, self).deserialize(raw)
        super(QGraphicsItemSerialization, self).deserialize(raw)

        if QGraphicObjectSerialization.validate(raw):
            self.setVisible(raw["visible"])
            self.setX(raw["x"])
            self.setY(raw["y"])
