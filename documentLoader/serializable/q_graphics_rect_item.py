# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentima -> q_graphics_rect_item.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 17/01/2022 23:38
# @Desc   :
# '''=================================================
from functools import reduce

from PySide2.QtCore import QRectF

from documentLoader.serializable.q_abstract_graphics_shape_item import QAbstractGraphicsShapeItemSerialization
from documentLoader.serializable.serializable import Serializable


class QGraphicsRectItemSerialization(QAbstractGraphicsShapeItemSerialization):
    REQUIRED_KEYS = ["rect"]

    def serialize(self):
        super_dict = super().serialize()
        this_dict = {
            "rect": [self.rect().x(), self.rect().y(), self.rect().width(),
                     self.rect().height()],
        }

        return reduce(Serializable.update, (super_dict, this_dict), {})

    def deserialize(self, raw):
        super().deserialize(raw)
        if QGraphicsRectItemSerialization.validate(raw):
            self.setRect(QRectF(*raw["rect"]))
