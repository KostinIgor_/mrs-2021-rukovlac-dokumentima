# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentima -> Q_graphics_scene.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 17/01/2022 10:27
# @Desc   :
# '''=================================================
from functools import reduce

from PySide2.QtCore import Qt, QRectF
from PySide2.QtGui import QBrush, QColor

from documentLoader.serializable.q_object import QObjectSerialization
from documentLoader.serializable.serializable import Serializable


class QGraphicsSceneSerialization(QObjectSerialization):
    REQUIRED_KEYS = ["backgroundBrush", "bspTreeDepth", "focusOnTouch", "foregroundBrush", "minimumRenderSize"
        , "sceneRect", "stickyFocus"]

    # itemIndexMethod ,

    def serialize(self):
        super_dict = super().serialize()
        this_dict = {
            "backgroundBrush":   self.backgroundBrush().color().rgba(),
            "bspTreeDepth":      self.bspTreeDepth(),
            "focusOnTouch":      self.focusOnTouch(),
            # "font":              self.font(),
            "foregroundBrush":   self.foregroundBrush().color().rgba(),
            "minimumRenderSize": self.minimumRenderSize(),
            # "palette":           self.palette(),
            "sceneRect":         [self.sceneRect().x(), self.sceneRect().y(), self.sceneRect().width(),
                                  self.sceneRect().height()],
            "stickyFocus":       self.stickyFocus()
        }

        return reduce(Serializable.update, (super_dict, this_dict), {})

    def deserialize(self, raw):
        super().deserialize(raw)
        if QGraphicsSceneSerialization.validate(raw):
            self.setBackgroundBrush(QBrush(QColor().fromRgba(raw["backgroundBrush"])))
            self.setBspTreeDepth(raw["bspTreeDepth"])
            self.setFocusOnTouch(raw["focusOnTouch"])
            # self.setFont(raw["font"])
            self.setForegroundBrush(QBrush(QColor().fromRgba(raw["foregroundBrush"])) if raw[
                                                                                             "foregroundBrush"] != 4278190080 else Qt.NoBrush)
            self.setMinimumRenderSize(raw["minimumRenderSize"])
            # self.setPalette(QDataStream() >> raw["palette"])
            self.setSceneRect(QRectF(*raw["sceneRect"]))
            self.setStickyFocus(raw["stickyFocus"])
