# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentima -> q_graphics_text_item.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 18/01/2022 16:29
# @Desc   :
# '''=================================================
from functools import reduce

from PySide2.QtGui import QColor

from documentLoader.serializable.q_graphics_object import QGraphicObjectSerialization
from documentLoader.serializable.serializable import Serializable


class QGraphicsTextItemSerialization(QGraphicObjectSerialization):
    REQUIRED_KEYS = [
        "openExternalLinks",
        # "textCursor",
        "plainText",
        "textWidth",
        "defaultTextColor"
    ]

    def serialize(self):
        super_dict = super().serialize()
        this_dict = {
            "openExternalLinks": self.openExternalLinks(),
            # "textCursor": CursorShape.extract(self.textCursor()),
            "plainText":         self.toPlainText(),
            "textWidth":         self.textWidth(),
            "defaultTextColor":  self.defaultTextColor().rgba(),
        }

        return reduce(Serializable.update, (super_dict, this_dict), {})

    def deserialize(self, raw):
        super().deserialize(raw)
        if QGraphicsTextItemSerialization.validate(raw):
            self.setOpenExternalLinks(raw["openExternalLinks"])
            # self.setTextCursor(QCursor(CursorShape.convert(raw["textCursor"])))
            print(f"Postaljam text na : {raw['plainText']}")
            self.setPlainText(raw["plainText"])
            self.setTextWidth(raw["textWidth"])
            self.setDefaultTextColor(QColor().fromRgba(raw["defaultTextColor"]))
