# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentima -> q_object_serialization.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 15/01/2022 10:49
# @Desc   : QObject
# '''=================================================

from documentLoader.serializable.serializable import Serializable


class QObjectSerialization(Serializable):
    REQUIRED_KEYS = ["objectName"]

    def serialize(self):
        return {
            "objectName": self.objectName()
        }

    def deserialize(self, raw):
        if QObjectSerialization.validate(raw):
            self.setObjectName(raw["objectName"])
