# -*- coding: UTF-8 -*-
# '''=================================================
# @Project -> File :mrs-2021-rukovlac-dokumentima -> serializable.py
# @IDE    :PyCharm
# @Author : Igor Kostin - (https://gitlab.com/KostinIgor_)
# @Time   : 15/01/2022 10:42
# @Desc   : Main interface for serializable objects
# '''=================================================
from abc import abstractmethod


class Serializable:
    @abstractmethod
    def serialize(self): ...

    @abstractmethod
    def deserialize(self, raw): ...

    @classmethod
    def validate(cls, raw: dict) -> bool:
        """
        Validate forwarded object ( raw ) for a given class (cls),
        Checks that all REQUIRED_KEYS are in that objet ( raw ), so the dict
        can be used to serialization or deserialization.

        :param raw: python raw dict for cls
        :return: bool
        """
        # print(f"REQUIRED_KEYS for - {cls} are {cls.REQUIRED_KEYS}")
        return all(key in raw for key in cls.REQUIRED_KEYS)

    @staticmethod
    def update(d, other):
        d.update(other)
        return d
