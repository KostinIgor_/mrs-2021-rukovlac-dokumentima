import sys
from pathlib import Path

from PySide2 import QtWidgets, QtCore

from __mainTets import Scene
from app.UI.main_window import MainWindow
from app.plugin_framework.manager.manager import PluginManager
from app.plugin_framework.reqistry.config.default import DefaultPluginConfigurator
from app.plugin_framework.reqistry.loaded.LoadedPluginsControl import LoadedPluginsControl
from app.plugin_framework.reqistry.loader.default_plugin_loader import DefaultPluginLoader
from app.plugin_framework.reqistry.plugin_registry import PluginRegistry
from app.plugin_framework.reqistry.repository.compund_plugin_repository import CompoundPluginRepository
from app.plugin_framework.reqistry.repository.file_system_plugin_repositroy import FileSystemPluginRepository
from app.UI.structure_dock import StructureDock
from app.UI.settings import Settings

if __name__ == "__main__":
    # TODO: aplikacija
    application = QtWidgets.QApplication(sys.argv)

    main_window = MainWindow("Rukovalac dokumentima")
    window=Settings(main_window)

    # REGISTRY creation
    cp = CompoundPluginRepository([FileSystemPluginRepository(Path("app/plugins/install"))])
    cp.refresh()
    plugins_control = LoadedPluginsControl()
    plugin_config = DefaultPluginConfigurator()
    loader = DefaultPluginLoader(plugin_config.config)


    plugin_registry = PluginRegistry(Path("app/plugins/install"),
                                     main_window,
                                     cp,
                                     plugins_control,
                                     plugin_config,
                                     loader)
    manager = PluginManager(plugins_control.model)

    main_doc = manager.get_by_extension_point(1516565165465)[0]

    main_doc.activate(manager, [])
    # main_doc.set([Scene.deserialize(sc, manager) for sc in main_doc.load()])

    structure_dock = StructureDock("Strukture dokumenata", main_window)
    main_window.addDockWidget(QtCore.Qt.LeftDockWidgetArea, structure_dock)

    main_window.menu_bar.clicked.connect(
        lambda path: main_doc.set([Scene.deserialize(sc, manager) for sc in main_doc.load(path)], path))

    main_window.menu_bar.new.connect(
        lambda : main_doc.set([], None))

    structure_dock.clicked.connect(
        lambda path: main_doc.set([Scene.deserialize(sc, manager) for sc in main_doc.load(path)], path))

    main_window.menu_bar.save.connect(lambda: main_doc.save())
    main_window.menu_bar.delete.connect(lambda: main_doc.delete())



    main_window.show()
    sys.exit(application.exec_())
