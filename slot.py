from functools import reduce

import PySide2
from PySide2.QtCore import Qt, QRectF, QPointF
from PySide2.QtGui import QBrush, QPainterPath, QPainter, QColor, QPen, QDragEnterEvent, \
    QDragMoveEvent, QDropEvent, QFocusEvent
from PySide2.QtWidgets import QGraphicsRectItem, QGraphicsItem, QWidget, \
    QPushButton, QColorDialog, QMenu, QAction, QMessageBox
from multimethod import multimethod

from app.plugin_framework.subscription.subscription import Subscription
from app.plugins.install.DocumentSimpleTextElement.plugin import ElementToolbarAction
# Izmene: 39,40,41, .. 159, 169, 184, 204, 219, 229, 244
# 159, 169, 184 .... vrsi se promena rect, za item prilikom izmena slota
from app.plugins.install.document.panel import Settings
from documentLoader.serializable.q_graphics_rect_item import QGraphicsRectItemSerialization
from documentLoader.serializable.serializable import Serializable


class SlotSettings(Settings):
    def __init__(self, slot, parent=None):
        super().__init__(parent, "Slot Settings")
        self.slot = slot
        self.settings = None
        self._opened = False

        self.setBackgroundBrushColur = QPushButton("Set background color")
        self.setBackgroundBrushColur.clicked.connect(lambda: self.set_background_color())

        self.settings_layout.addWidget(self.setBackgroundBrushColur)

        self._settings.hide()

    def set_background_color(self):
        color = QColorDialog(self._main).getColor()
        if color.isValid():
            self.slot.setBrush(QBrush(Qt.red))


class Slot(QGraphicsRectItem, Subscription, QGraphicsRectItemSerialization):
    handleTopLeft = 1
    handleTopMiddle = 2
    handleTopRight = 3
    handleMiddleLeft = 4
    handleMiddleRight = 5
    handleBottomLeft = 6
    handleBottomMiddle = 7
    handleBottomRight = 8

    handleSize = +8.0
    handleSpace = -4.0

    handleCursors = {
        handleTopLeft:      Qt.SizeFDiagCursor,
        handleTopMiddle:    Qt.SizeVerCursor,
        handleTopRight:     Qt.SizeBDiagCursor,
        handleMiddleLeft:   Qt.SizeHorCursor,
        handleMiddleRight:  Qt.SizeHorCursor,
        handleBottomLeft:   Qt.SizeBDiagCursor,
        handleBottomMiddle: Qt.SizeVerCursor,
        handleBottomRight:  Qt.SizeFDiagCursor,
    }

    REQUIRED_KEYS = ["item", "prevx", "prevy", "prevxMove", "prevyMove",
                     "prevh", "prevw", "resizing"]

    def __init__(self, x, y, width, height, item=None, parent=None, manager=None):
        super().__init__(x, y, width, height, parent)

        self.painter = None
        self.setAcceptDrops(True)

        self.item = item
        if item is not None:
            self._add(item)
        self.manager = manager
        self._settings = None
        self._subscriptions = []

        self.handles = {}
        self.handleSelected = None
        self.mousePressPos = None
        self.mousePressRect = None
        self.setAcceptHoverEvents(True)
        self.setFlag(QGraphicsItem.ItemIsMovable, True)
        self.setFlag(QGraphicsItem.ItemIsSelectable, True)
        self.setFlag(QGraphicsItem.ItemSendsGeometryChanges, True)
        self.setFlag(QGraphicsItem.ItemIsFocusable, True)
        self.updateHandlesPos()
        self.prevx = self.boundingRect().x()
        self.prevy = self.boundingRect().y()
        self.prevxMove = self.x()
        self.prevyMove = self.y()
        self.prevh = height
        self.prevw = width
        self.resizing = False

    def mouseDoubleClickEvent(self, event: PySide2.QtWidgets.QGraphicsSceneMouseEvent) -> None:
        # if self.focusItem() is not None:
        #     print(f"Focused item in SLOT is : {self.focusItem()}")
        #     self.focusItem().mouseDoubleClickEvent(event)
        #     return
        print("Double click slot")

    def handleAt(self, point):
        """
        Returns the resize handle below the given point.
        """
        for k, v, in self.handles.items():
            if v.contains(point):
                return k
        return None

    def hoverMoveEvent(self, moveEvent):
        """
        Executed when the mouse moves over the shape (NOT PRESSED).
        """
        if self.isSelected():
            handle = self.handleAt(moveEvent.pos())
            cursor = Qt.ArrowCursor if handle is None else self.handleCursors[handle]
            self.setCursor(cursor)
        super().hoverMoveEvent(moveEvent)

    def hoverLeaveEvent(self, moveEvent):
        """# promena pozicije itema
        Executed when the mouse leaves the shape (NOT PRESSED).
        """
        self.setCursor(Qt.ArrowCursor)
        super().hoverLeaveEvent(moveEvent)

    def mousePressEvent(self, mouseEvent):
        """
        Executed when the mouse is pressed on the item.
        """
        # if mouseEvent.button() == Qt.RightButton:
        #     self.show_contextMenu(mouseEvent.pos())
        #     return
        self.handleSelected = self.handleAt(mouseEvent.pos())
        if self.handleSelected:
            self.mousePressPos = mouseEvent.pos()
            self.mousePressRect = self.boundingRect()
        super().mousePressEvent(mouseEvent)

    def mouseMoveEvent(self, mouseEvent):
        """
        Executed when the mouse is being moved over the item while being pressed.
        """
        if self.handleSelected is not None:
            self.resizing = True
            self.interactiveResize(mouseEvent.pos())
        else:
            super().mouseMoveEvent(mouseEvent)

    def mouseReleaseEvent(self, mouseEvent):
        """
        Executed when the mouse is released from the item.
        """

        super().mouseReleaseEvent(mouseEvent)
        colliding = self.collidingItems()
        for item in colliding:
            if item is not self.item:
                print(self.resizing)
                if not self.resizing:
                    newx = self.prevxMove - self.x()
                    newy = self.prevyMove - self.y()
                    self.moveBy(newx, newy)
                else:
                    self._setRect(QRectF(self.prevx,self.prevy, self.prevw,self.prevh))
                    self.resizing = False
                    self.updateHandlesPos()
                    self.update(self.boundingRect())
                    self.prevxMove = self.x()
                    self.prevyMove = self.y()
                    return
        self.prevh = self.rect().height()
        self.prevw = self.rect().width()
        self.prevxMove = self.x()
        self.prevyMove = self.y()
        self.handleSelected = None
        self.mousePressPos = None
        self.mousePressRect = None
        self.update()

    def moveCollisionHandler(self):
        pass

    def resizeCollisionHandler(self):
        pass

    def boundingRect(self):
        """
        Returns the bounding rect of the shape (including the resize handles).
        """
        o = self.handleSize + self.handleSpace
        return self.rect().adjusted(-o, -o, o, o)

    def updateHandlesPos(self):
        """
        Update current resize handles according to the shape size and position.
        """
        s = self.handleSize
        b = self.boundingRect()
        self.handles[self.handleTopLeft] = QRectF(b.left(), b.top(), s, s)
        self.handles[self.handleTopMiddle] = QRectF(b.center().x() - s / 2, b.top(), s, s)
        self.handles[self.handleTopRight] = QRectF(b.right() - s, b.top(), s, s)
        self.handles[self.handleMiddleLeft] = QRectF(b.left(), b.center().y() - s / 2, s, s)
        self.handles[self.handleMiddleRight] = QRectF(b.right() - s, b.center().y() - s / 2, s, s)
        self.handles[self.handleBottomLeft] = QRectF(b.left(), b.bottom() - s, s, s)
        self.handles[self.handleBottomMiddle] = QRectF(b.center().x() - s / 2, b.bottom() - s, s, s)
        self.handles[self.handleBottomRight] = QRectF(b.right() - s, b.bottom() - s, s, s)

    def _setRect(self, rect: QRectF) -> None:
        self.setRect(rect)
        self.adjust_items()

    def interactiveResize(self, mousePos):
        """
        Perform shape interactive resize.
        """  # promena pozicije itema
        offset = self.handleSize + self.handleSpace
        boundingRect = self.boundingRect()
        rect = self.rect()
        diff = QPointF(0, 0)

        self.prepareGeometryChange()

        if self.handleSelected == self.handleTopLeft:

            fromX = self.mousePressRect.left()
            fromY = self.mousePressRect.top()
            toX = fromX + mousePos.x() - self.mousePressPos.x()
            toY = fromY + mousePos.y() - self.mousePressPos.y()
            diff.setX(toX - fromX)
            diff.setY(toY - fromY)
            boundingRect.setLeft(toX)
            boundingRect.setTop(toY)
            rect.setLeft(boundingRect.left() + offset)
            rect.setTop(boundingRect.top() + offset)
            self._setRect(rect)

        elif self.handleSelected == self.handleTopMiddle:

            fromY = self.mousePressRect.top()
            toY = fromY + mousePos.y() - self.mousePressPos.y()
            diff.setY(toY - fromY)
            boundingRect.setTop(toY)
            rect.setTop(boundingRect.top() + offset)
            self._setRect(rect)

        elif self.handleSelected == self.handleTopRight:

            fromX = self.mousePressRect.right()
            fromY = self.mousePressRect.top()
            toX = fromX + mousePos.x() - self.mousePressPos.x()
            toY = fromY + mousePos.y() - self.mousePressPos.y()
            diff.setX(toX - fromX)
            diff.setY(toY - fromY)
            boundingRect.setRight(toX)
            boundingRect.setTop(toY)
            rect.setRight(boundingRect.right() - offset)
            rect.setTop(boundingRect.top() + offset)
            self._setRect(rect)

        elif self.handleSelected == self.handleMiddleLeft:

            fromX = self.mousePressRect.left()
            toX = fromX + mousePos.x() - self.mousePressPos.x()
            diff.setX(toX - fromX)
            boundingRect.setLeft(toX)
            rect.setLeft(boundingRect.left() + offset)
            self._setRect(rect)

        elif self.handleSelected == self.handleMiddleRight:
            # print("MR")
            fromX = self.mousePressRect.right()
            toX = fromX + mousePos.x() - self.mousePressPos.x()
            diff.setX(toX - fromX)
            boundingRect.setRight(toX)
            rect.setRight(boundingRect.right() - offset)
            self._setRect(rect)

        elif self.handleSelected == self.handleBottomLeft:

            fromX = self.mousePressRect.left()
            fromY = self.mousePressRect.bottom()
            toX = fromX + mousePos.x() - self.mousePressPos.x()
            toY = fromY + mousePos.y() - self.mousePressPos.y()
            diff.setX(toX - fromX)
            diff.setY(toY - fromY)
            boundingRect.setLeft(toX)
            boundingRect.setBottom(toY)
            rect.setLeft(boundingRect.left() + offset)
            rect.setBottom(boundingRect.bottom() - offset)
            self._setRect(rect)

        elif self.handleSelected == self.handleBottomMiddle:

            fromY = self.mousePressRect.bottom()
            toY = fromY + mousePos.y() - self.mousePressPos.y()
            diff.setY(toY - fromY)
            boundingRect.setBottom(toY)
            rect.setBottom(boundingRect.bottom() - offset)
            self._setRect(rect)

        elif self.handleSelected == self.handleBottomRight:

            fromX = self.mousePressRect.right()
            fromY = self.mousePressRect.bottom()
            toX = fromX + mousePos.x() - self.mousePressPos.x()
            toY = fromY + mousePos.y() - self.mousePressPos.y()
            diff.setX(toX - fromX)
            diff.setY(toY - fromY)
            boundingRect.setRight(toX)
            boundingRect.setBottom(toY)
            rect.setRight(boundingRect.right() - offset)
            rect.setBottom(boundingRect.bottom() - offset)
            self._setRect(rect)

        self.updateHandlesPos()

    def shape(self):
        """
        Returns the shape of this item as a QPainterPath in local coordinates.
        """
        path = QPainterPath()
        path.addRect(self.rect())
        if self.isSelected():
            for shape in self.handles.values():
                path.addEllipse(shape)
        return path

    def paint(self, painter, option, widget=QWidget):
        """
        Paint the node in the graphic view.
        """
        self.painter = painter
        painter.setBrush(QBrush(QColor(255, 0, 0, 100)))
        painter.setPen(QPen(QColor(0, 0, 0), 1.0, Qt.SolidLine))
        painter.drawRect(self.rect())
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setBrush(QBrush(QColor(255, 0, 0, 255)))
        painter.setPen(QPen(QColor(0, 0, 0, 255), 1.0, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin))
        for handle, rect in self.handles.items():
            if self.handleSelected is None or handle == self.handleSelected:
                painter.drawEllipse(rect)

    # DROP_EVENT
    def dragEnterEvent(self, event: QDragEnterEvent) -> None:
        if self.item is not None:
            event.ignore()
            return

        if event.mimeData().hasFormat(ElementToolbarAction.DEFAULT_EXTENSION_GROUP):
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event: QDragMoveEvent) -> None:
        if event.mimeData().hasFormat(ElementToolbarAction.DEFAULT_EXTENSION_GROUP):
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event: QDropEvent) -> None:
        if event.mimeData().hasFormat(ElementToolbarAction.DEFAULT_EXTENSION_GROUP):
            self._add((self.manager.get(event.mimeData().id).element(self))())
            event.accept()
        else:
            event.ignore()

    def _add(self, item):
        if item is None and self.item is None:
            return

        if item is None and self.item is not None:
            choice = QMessageBox.question(None, 'Override', 'Are u sure you want to delete item in this slot?',
                                          QMessageBox.Ok | QMessageBox.Cancel)

            if choice == QMessageBox.Ok:
                self.item.setParentItem(None)
                self.item.hide()
                self.item = None
            return

        if self.item is not None:
            choice = QMessageBox.question(None, 'Override', 'Are u sure you want to override item in this slot?',
                                          QMessageBox.Ok | QMessageBox.Cancel)

            if choice == QMessageBox.Ok:
                self.item.setParentItem(None)
                self.item.hide()
                self.item = None
            else:
                return

        self.item = item
        self.item.setParentItem(self)
        self.adjust_items()

        item.setFlag(QGraphicsItem.ItemIsMovable, False)

    def adjust_items(self):
        if self.item is None:
            return

        self.item.setRect(self.rect().x(),
                          self.rect().y(),
                          self.rect().size().width(),
                          self.rect().size().height())

        # OFFSET PARAMETERS
        # w = self.rect().size().width()
        # h = self.rect().size().height()
        # iw = self.item.rect().size().width()
        # ih = self.item.rect().size().height()

        # CENTER VERTICALLY
        # offset = (w - iw) / 2
        #
        # self.item.setPos(self.rect().x() + offset, self.rect().y())

        # CENTER HORIZONTALLY
        # offset = (h - ih) / 2
        #
        # self.item.setPos(self.rect().x(), self.rect().y() + offset)

        # CENTER HORIZONTALLY AND VERTICALLY
        # offset_W = (w - iw) / 2
        # offset_H = (h - ih) / 2
        #
        # self.item.setPos(self.rect().x() + offset_W, self.rect().y() + offset_H)

    def focusInEvent(self, event: QFocusEvent) -> None:
        # print("SlotFocusInEvent")
        self._emit(QFocusEvent.FocusIn, [*[self.item.settings if self.item is not None else None], self.settings])

    # def focusOutEvent(self, event: QFocusEvent) -> None:
    #     print("SlotFocusOutEvent")
    #     self._emit(QFocusEvent.FocusOut, [self.settings, *[self.item.settings if self.item is not None else []]])

    @property
    def settings(self):
        if self._settings is None:
            self._settings = SlotSettings(self)
            self._settings._title.setText(f"Slot {self.boundingRect().x()}")
        return self._settings

    def serialize(self):
        super_dict = super().serialize()
        this_dict = {
            "item":      self.item.serialize() if self.item is not None else None,
            "prevx":     self.prevx,
            "prevy":     self.prevy,
            "prevxMove": self.prevxMove,
            "prevyMove": self.prevyMove,
            "prevh":     self.prevh,
            "prevw":     self.prevw,
            "resizing":  self.resizing
        }

        return reduce(Serializable.update, (super_dict, this_dict), {})

    @staticmethod
    @multimethod
    def deserialize(raw, parent, manager):
        slot = Slot(0, 0, 0, 0, None, parent, manager)
        slot._deserialize(raw)

        return slot

    @multimethod
    def _deserialize(self, raw: dict):
        super().deserialize(raw)
        if Slot.validate(raw):
            if raw["item"] is not None:
                self._add((self.manager.get(int(raw["item"]["id"])).element(self)).deserialize(raw["item"]))
            self.prevx = raw["prevx"]
            self.prevy = raw["prevy"]
            self.prevxMove = raw["prevxMove"]
            self.prevYMove = raw["prevyMove"]
            self.prevh = raw["prevh"]
            self.prevw = raw["prevw"]
            self.resizing = raw["resizing"]

        self.updateHandlesPos()
        self.update()

    def contextMenuEvent(self, event: PySide2.QtWidgets.QGraphicsSceneContextMenuEvent) -> None:
        menu = QMenu()
        delete = QAction("Delete", menu)
        delete.triggered.connect(lambda: self._emit("DeleteItem", self))
        menu.addAction(delete)

        set = QMenu("Set Item", menu)
        indexes = []
        for index, _item in enumerate(self.manager.get_by_extension_point(4546516565)):
            a = QAction(_item.name, set)
            indexes.append(_Open(_item.id, self._add, self.manager))
            a.triggered.connect(indexes[index].run)
            set.addAction(a)

        a = QAction("Delete Item", set)
        a.triggered.connect(lambda: self._add(None))
        set.addAction(a)

        menu.addMenu(set)
        menu.exec_(event.screenPos())


class _Open:
    def __init__(self, _id, callback, manager):
        self._id = _id
        self.manager = manager
        self.callback = callback

    def run(self):
        print(self._id)
        self.callback((self.manager.get(self._id).element(self))())
